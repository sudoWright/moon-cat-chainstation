import { MoonCatData } from 'lib/types'
import { Address, useContractReads } from 'wagmi'
import { parseAbi } from 'viem'
import LoadingIndicator from './LoadingIndicator'
import { useIsMounted } from 'lib/util'
import WarningIndicator from './WarningIndicator'
import EthereumAddress from './EthereumAddress'
import { TabProps } from 'pages/mooncats/[id]'

const TRAITS = {
  address: '0x9330BbfBa0C8FdAf0D93717E4405a410a6103cC2' as Address,
  abi: parseAbi([
    'function traitsOf(uint256 rescueIndex) external view returns (bool genesis, bool pale, string facing, string expression, string pattern, string pose, bytes5 catId, uint16 rescueYear, bool isNamed)',
    'function nameOf(uint256 rescueIndex) public view returns (string memory)',
  ]),
}

const COLORS = {
  address: '0x2fd7E0c38243eA15700F45cfc38A7a7f66df1deC' as Address,
  abi: parseAbi(['function colorsOf(uint256 rescueOrder) external view returns (uint8[24])']),
}

const SVG = {
  address: `0xB39C61fe6281324A23e079464f7E697F8Ba6968f` as Address,
  abi: parseAbi(['function imageOf(uint256 rescueOrder) external view returns (string)']),
}

const ACCESSORIZED = {
  address: `0x91CF36c92fEb5c11D3F5fe3e8b9e212f7472Ec14` as Address,
  abi: parseAbi(['function accessorizedImageOf(uint256 rescueOrder) external view returns (string)']),
}

const ColorChip = ({ label, color }: { label: string; color: string }) => {
  return <div title={label} style={{ flexGrow: 1, background: color }} />
}

const MoonCatTabOnChain = ({ moonCat }: TabProps) => {
  const isMounted = useIsMounted()
  const { data, isError, isLoading } = useContractReads({
    contracts: [
      {
        ...TRAITS,
        functionName: 'traitsOf',
        args: [BigInt(moonCat.rescueOrder)],
      },
      {
        ...TRAITS,
        functionName: 'nameOf',
        args: [BigInt(moonCat.rescueOrder)],
      },
      {
        ...COLORS,
        functionName: 'colorsOf',
        args: [BigInt(moonCat.rescueOrder)],
      },
      {
        ...SVG,
        functionName: 'imageOf',
        args: [BigInt(moonCat.rescueOrder)],
      },
      {
        ...ACCESSORIZED,
        functionName: 'accessorizedImageOf',
        args: [BigInt(moonCat.rescueOrder)],
      },
    ],
    allowFailure: false,
  })

  if (isLoading || !isMounted) {
    return (
      <section className="card">
        <h2>On-Chain Details</h2>
        <LoadingIndicator />
      </section>
    )
  }
  if (isError || typeof data == 'undefined') {
    return (
      <section className="card">
        <h2>On-Chain Details</h2>
        <WarningIndicator message="Failed to fetch on-chain data" />
      </section>
    )
  }

  const colors = {
    glow: `rgb(${data[2][0]},${data[2][1]},${data[2][2]})`,
    border: `rgb(${data[2][3]},${data[2][4]},${data[2][5]})`,
    pattern: `rgb(${data[2][6]},${data[2][7]},${data[2][8]})`,
    coat: `rgb(${data[2][9]},${data[2][10]},${data[2][11]})`,
    belly: `rgb(${data[2][12]},${data[2][13]},${data[2][14]})`,
    nose: `rgb(${data[2][15]},${data[2][16]},${data[2][17]})`,
    comp1: `rgb(${data[2][18]},${data[2][19]},${data[2][20]})`,
    comp2: `rgb(${data[2][21]},${data[2][22]},${data[2][23]})`,
  }

  return (
    <>
      <div className="text-container">
        <section className="card-help">
          The core traits of each MoonCat are enshrined forever in the Ethereum blockchain (i.e. you don&rsquo;t have to
          trust this website, to verify what a MoonCat is). Here&rsquo;s how to find that data, in a set of contracts
          designed for that purpose:
        </section>
        <section className="card">
          <h3>MoonCatTraits</h3>
          <p>
            Raw result of calling <code>traitsOf({moonCat.rescueOrder})</code> on{' '}
            <EthereumAddress address={TRAITS.address} linkProfile={false} />: <code>{data[0].join(', ')}</code>
          </p>
          <p>Parsing that list of results gives:</p>
          <ul style={{ columnWidth: '18em' }}>
            <li>
              <strong>Genesis</strong> {data[0][0] ? 'Yes' : 'No'}
            </li>
            <li>
              <strong>Pale</strong> {data[0][1] ? 'Yes' : 'No'}
            </li>
            <li>
              <strong>Facing</strong> {data[0][2]}
            </li>
            <li>
              <strong>Expression</strong> {data[0][3]}
            </li>
            <li>
              <strong>Pattern</strong> {data[0][4]}
            </li>
            <li>
              <strong>Pose</strong> {data[0][5]}
            </li>
            <li>
              <strong>Hex ID</strong> {data[0][6]}
            </li>
            <li>
              <strong>Rescue Year</strong> {data[0][7]}
            </li>
            <li>
              <strong>Is Named?</strong> {data[0][8] ? 'Yes' : 'No'}
            </li>
          </ul>
          {data[0][8] && (
            <p>
              Raw result of calling <code>nameOf({moonCat.rescueOrder})</code> on{' '}
              <EthereumAddress address={TRAITS.address} linkProfile={false} />: <code>{data[1]}</code>
            </p>
          )}

          <h3 style={{ marginTop: '4rem' }}>MoonCatColors</h3>
          <p>
            Raw result of calling <code>colorsOf({moonCat.rescueOrder})</code> on{' '}
            <EthereumAddress address={COLORS.address} linkProfile={false} />: <code>[{data[2].join(', ')}]</code>
          </p>
          <p>Parsing that list into RGB triples gives:</p>
          <ul style={{ columnWidth: '23em' }}>
            <li>
              <strong>Glow</strong> <code>{colors.glow}</code>
            </li>
            <li>
              <strong>Border</strong> <code>{colors.border}</code>
            </li>
            <li>
              <strong>Pattern</strong> <code>{colors.pattern}</code>
            </li>
            <li>
              <strong>Coat</strong> <code>{colors.coat}</code>
            </li>
            <li>
              <strong>Belly/Mouth/Whiskers</strong> <code>{colors.belly}</code>
            </li>
            <li>
              <strong>Nose/Ears/Feet</strong> <code>{colors.nose}</code>
            </li>
            <li>
              <strong>Complement #1</strong> <code>{colors.comp1}</code>
            </li>
            <li>
              <strong>Complement #2</strong> <code>{colors.comp2}</code>
            </li>
          </ul>
          <div style={{ height: 30, display: 'flex' }}>
            <ColorChip label="Glow" color={colors.glow} />
            <ColorChip label="Border" color={colors.border} />
            <ColorChip label="Pattern" color={colors.pattern} />
            <ColorChip label="Coat" color={colors.coat} />
            <ColorChip label="Belly/Mouth/Whiskers" color={colors.belly} />
            <ColorChip label="Nose/Ears/Feet" color={colors.nose} />
            <ColorChip label="Complement #1" color={colors.comp1} />
            <ColorChip label="Complement #2" color={colors.comp2} />
          </div>

          <div style={{ marginTop: '3rem', display: 'flex', gap: '2rem', flexWrap: 'wrap' }}>
            <div style={{ flex: '1 1 40%', minWidth: '23em' }}>
              <h3>MoonCatSVGs</h3>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <div style={{ flex: '1 1 auto' }}>
                  <p>
                    Calling <code>imageOf({moonCat.rescueOrder})</code> on{' '}
                    <EthereumAddress address={SVG.address} linkProfile={false} /> gives the SVG:
                  </p>
                </div>
                <div style={{ flex: '1 1 100px', padding: '0 1rem', textAlign: 'center' }}>
                  <img style={{ height: 100 }} src={`data:image/svg+xml;utf8,${encodeURIComponent(data[3])}`} />
                </div>
              </div>
            </div>
            <div style={{ flex: '1 1 40%', minWidth: '28em' }}>
              <h3>MoonCatAccessoryImages</h3>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <div style={{ flex: '1 1 auto' }}>
                  <p>
                    Calling <code>accessorizedImageOf({moonCat.rescueOrder})</code> on{' '}
                    <EthereumAddress address={ACCESSORIZED.address} linkProfile={false} /> gives the SVG:
                  </p>
                </div>
                <div style={{ flex: '1 1 100px', padding: '0 1rem', textAlign: 'center' }}>
                  <img style={{ height: 100 }} src={`data:image/svg+xml;utf8,${encodeURIComponent(data[4])}`} />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  )
}

export default MoonCatTabOnChain
