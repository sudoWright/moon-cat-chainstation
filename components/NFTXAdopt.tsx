import React, { CSSProperties, useEffect, useState } from 'react'
import { switchToChain, useFetchStatus } from 'lib/util'
import useTxStatus from 'lib/useTxStatus'
import Icon from 'components/Icon'
import LoadingIndicator from 'components/LoadingIndicator'
import { customEvent } from 'lib/analytics'
import { BaseError, formatEther, parseAbi } from 'viem'
import { useAccount } from 'wagmi'
import { prepareWriteContract, waitForTransaction, writeContract } from 'wagmi/actions'
import { useWeb3Modal } from '@web3modal/wagmi/react'

interface PriceQuote {
  calldata: `0x${string}`
  sellAmount: bigint
}

interface Props {
  tokenId?: number
  quoteType: string
  style?: CSSProperties
}

const NFTXAdopt = ({ tokenId, quoteType, style }: Props) => {
  const [priceQuote, setPriceQuote] = useState<PriceQuote>({
    calldata: '0x',
    sellAmount: 0n,
  })
  const [priceStatus, setPriceStatus] = useFetchStatus()
  const { status: adoptStatus, viewMessage: adoptMessage, setStatus: setAdoptStatus } = useTxStatus()
  const { address, isConnected } = useAccount()
  const { open } = useWeb3Modal()

  // On component mount, fetch the current price for swapping to this token
  useEffect(() => {
    setPriceStatus('pending')
    fetch(`/api/nftx-price/${quoteType}`)
      .then((rs) => {
        return rs.json()
      })
      .then((data) => {
        const q = data['WETH']
        setPriceQuote({
          calldata: q.calldata,
          sellAmount: BigInt(q.sellAmount),
        })
        setPriceStatus('done')
      })
      .catch((err) => {
        console.error(err)
        setPriceStatus('error')
      })
  }, [setPriceStatus, quoteType])

  if (priceStatus != 'done') {
    return <LoadingIndicator style={style} />
  }
  // Need to pad the amount, in case slippage happens before the swap confirms
  const actualPrice = (priceQuote.sellAmount * 102n) / 100n
  if (!isConnected) {
    const target = typeof tokenId == 'undefined' ? 'adopt now' : 'adopt them now'
    return (
      <section className="sub-card" style={style}>
        <p>
          <button onClick={(e) => open()}>Connect</button>
        </p>
        <p>
          Connect your wallet if you&rsquo;d like to {target} for{' '}
          <Icon name="ethereum" style={{ marginRight: '0.1em', verticalAlign: '-0.15em' }} />
          {formatEther(actualPrice).slice(0, 7)} ETH.
        </p>
      </section>
    )
  }

  const doAdopt = async () => {
    // User has clicked the Adopt button; prepare and send a transaction to adopt that MoonCat
    let eventConfig: Record<string, any> = {
      currency: 'ETH',
      value: actualPrice,
    }
    if (typeof tokenId == 'undefined') {
      eventConfig.items = [
        {
          item_name: 'Random MoonCat',
          affiliation: 'NFTX',
          item_brand: 'MoonCatRescue',
          item_category: 'MoonCat',
          item_variant: quoteType,
          quantity: 1,
        },
      ]
    } else {
      eventConfig.items = [
        {
          item_id: tokenId,
          item_name: 'MoonCat ' + tokenId,
          affiliation: 'NFTX',
          item_brand: 'MoonCatRescue',
          item_category: 'MoonCat',
          item_variant: quoteType,
          quantity: 1,
        },
      ]
    }
    customEvent('begin_checkout', eventConfig)

    // NFTXMarketplace0xZap contract: https://docs.nftx.io/smart-contracts/integrations#buyandredeem
    const config = {
      address: '0x941A6d105802CCCaa06DE58a13a6F49ebDCD481C' as `0x${string}`,
      abi: parseAbi([
        'function buyAndRedeem(uint256 vaultId, uint256 amount, uint256[] specificIds, bytes swapCallData, address to) external payable',
      ]),
    }
    setAdoptStatus('building')
    if (!(await switchToChain(1))) {
      setAdoptStatus('error', 'Wrong network chain')
      return
    }
    const specificIds = typeof tokenId == 'undefined' ? [] : [BigInt(tokenId)]

    let request
    try {
      const rs = await prepareWriteContract({
        ...config,
        functionName: 'buyAndRedeem',
        args: [25n, 1n, specificIds, priceQuote.calldata, address!],
        value: actualPrice,
      })
      request = rs.request
    } catch (err) {
      if (err instanceof BaseError) {
        console.error('Simulation failure', {
          name: err.name,
          shortMessage: err.shortMessage,
          cause: err.cause,
        })
        setAdoptStatus('error', err.shortMessage)
      } else {
        console.log('Simulation unknown error', err)
        setAdoptStatus('done')
      }
      return
    }
    const adoptPromise = writeContract(request)
    setAdoptStatus('authorizing')
    try {
      const { hash } = await adoptPromise
      setAdoptStatus('confirming')
      await waitForTransaction({ hash })
      setAdoptStatus(
        'done',
        <p>
          <strong>Adopted!</strong> This page will update shortly to mark you as the new owner.
        </p>
      )
      eventConfig.transaction_id = hash
      customEvent('purchase', eventConfig)
    } catch (err) {
      if (err instanceof BaseError) {
        console.error('Transaction failure', {
          name: err.name,
          shortMessage: err.shortMessage,
          cause: err.cause,
        })
        setAdoptStatus('error', err.shortMessage)
      } else {
        console.log('Transaction unknown error', err)
        setAdoptStatus('error', 'Transaction error')
      }
      return
    }
  }

  const isButtonDisabled = ['building', 'authorizing', 'confirming'].includes(adoptStatus)
  const target = typeof tokenId == 'undefined' ? 'Adopt!' : 'Adopt Me!'

  return (
    <section className="sub-card" style={style}>
      <p>
        <button onClick={doAdopt} disabled={isButtonDisabled}>
          {target}
        </button>{' '}
        for <Icon name="ethereum" style={{ marginRight: '0.1em', verticalAlign: '-0.15em' }} />
        {formatEther(actualPrice).slice(0, 7)} ETH{' '}
      </p>
      {adoptMessage}
    </section>
  )
}

export default NFTXAdopt
