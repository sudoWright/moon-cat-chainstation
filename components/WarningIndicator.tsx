import React from 'react'
import Icon from 'components/Icon'

interface Props {
  message: string
  className?: string
  style?: React.CSSProperties
}
const WarningIndicator = ({ message, className, style }: Props) => {
  let classes = ['message-error']
  if (typeof className != 'undefined') classes.push(className)
  return (
    <p style={style} className={classes.join(' ')}>
      <Icon name="warning" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
      {message}
    </p>
  )
}
export default WarningIndicator
