import React, { CSSProperties, useState } from 'react'
import LoadingIndicator from 'components/LoadingIndicator'
import WarningIndicator from 'components/WarningIndicator'
import { useFetchStatus } from 'lib/util'
import { Address } from 'wagmi'
import { fetchEnsAddress } from 'wagmi/actions'

interface Props {
  onValidate: (address: Address) => any
  style?: CSSProperties
}

const AddressLookup = ({ onValidate, style }: Props) => {
  const [addressInput, setInputAddress] = useState<string>('')
  const [errorMessage, setErrorMessage] = useState<string>('')
  const [fetchStatus, setFetchStatus] = useFetchStatus()

  async function doLookup() {
    setErrorMessage('')
    setFetchStatus('pending')
    if (addressInput.match(/^0x[0-9a-f]{40}$/i)) {
      // Valid hex address
      onValidate(addressInput as Address)
      return
    }

    let ensAddr = await fetchEnsAddress({ name: addressInput })
    if (typeof ensAddr != 'undefined' && ensAddr != null) {
      // Valid ENS name
      onValidate(ensAddr)
      return
    }

    setFetchStatus('error')
    setErrorMessage('That is not a valid address')
  }

  let fetchDisplay: React.ReactNode
  switch (fetchStatus) {
    case 'error': {
      fetchDisplay = <WarningIndicator style={{ textAlign: 'center' }} message={errorMessage} />
      break
    }
    case 'pending': {
      fetchDisplay = <LoadingIndicator style={{ textAlign: 'center' }} message="Searching..." />
      break
    }
  }

  return (
    <div className="address-lookup" style={style}>
      <p style={{ textAlign: 'center' }}>
        <input
          type="text"
          value={addressInput}
          onKeyDown={(e) => {
            if (e.key == 'Enter') doLookup()
          }}
          onChange={(e) => setInputAddress(e.target.value)}
          className="address-search"
          style={{ marginRight: '1em' }}
          placeholder="Ethereum address"
          autoCorrect="off"
          autoCapitalize="off"
          spellCheck="false"
        />
        <button
          onClick={(e) => {
            e.preventDefault()
            doLookup()
          }}
        >
          Lookup
        </button>
      </p>
      {fetchDisplay}
    </div>
  )
}
export default AddressLookup
