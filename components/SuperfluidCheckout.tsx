import { useWeb3Modal, useWeb3ModalState } from '@web3modal/wagmi/react'
import SuperfluidWidget, { WidgetProps } from '@superfluid-finance/widget'
import superTokenList from '@superfluid-finance/tokenlist'
import _rawData from 'lib/superfluid_widget_data.json'

const SuperfluidCheckout = ({}) => {
  const { open } = useWeb3Modal()
  const { open: isOpen } = useWeb3ModalState()

  const data = _rawData as WidgetProps

  return (
    <section id="superfluid-checkout">
      <p>
        The most direct way you can financially help the MoonCatRescue team continue to create great content and grow
        the MoonCat Ecosystem is donations directly to the team. Lump-sum donations can be sent to{' '}
        <code>team.mooncatrescue.eth</code>{' '}
        <span style={{ whiteSpace: 'nowrap' }}>
          (<code>0xdf2E60Af57C411F848B1eA12B10a404d194bce27</code>).
        </span>{' '}
        While donations of any sort are always appreciated, making your contribution a{' '}
        <a href="https://www.superfluid.finance/" target="_blank" rel="noreferrer">
          Superfluid
        </a>{' '}
        subscription would help us make our finances a bit more predictable. A Superfluid subscription streams your
        donation over time, allowing you to create a monthly sponsorship that pays out automatically over time. Click
        the button below to pull up a widget to help you set that up.
      </p>
      <p>
        <SuperfluidWidget {...data} tokenList={superTokenList} type="drawer" walletManager={{ open, isOpen} as any}>
          {({ openModal }) => <button onClick={() => openModal()}>Open Superfluid Widget</button>}
        </SuperfluidWidget>
      </p>
      <p>
        The MoonCatRescue Team&rsquo;s current sponsorship goal is to have enough sponsors to support our two team
        members at $200/month. That&rsquo;s a very low payment for the skill and time the team puts in, but it&rsquo;s a
        good first benchmark to find sponsors for. You can track the team&rsquo;s progress on making that cash flow
        balance in the{' '}
        <a
          href="https://console.superfluid.finance/arbitrum-one/accounts/0xf45349435c4A7cbb519987bd4d7460479568fD63?tab=map"
          target="_blank"
          rel="noreferrer"
        >
          Superfluid console
        </a>
        .
      </p>
    </section>
  )
}

export default SuperfluidCheckout
