import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Icon from './Icon'
import LoadingIndicator from './LoadingIndicator'
import { TabProps } from '../pages/mooncats/[id]'
import { Moment, MoonCatData, OwnedMoonCat } from 'lib/types'
import {
  ACCLIMATOR_ADDRESS,
  API_SERVER_ROOT,
  API2_SERVER_ROOT,
  IPFS_GATEWAY,
  MOMENTS_ADDRESS,
  ZWS,
  switchToChain,
  useFetchStatus,
} from 'lib/util'
import { multicall, readContract } from 'wagmi/actions'
import { parseAbi, sha256 } from 'viem'
import { useWeb3Modal } from '@web3modal/wagmi/react'
import useSignedIn from 'lib/useSignedIn'
import WarningIndicator from './WarningIndicator'
import useTxStatus from 'lib/useTxStatus'
const moments: Moment[] = require('lib/moments_meta.json')

const ACCLIMATOR = {
  address: ACCLIMATOR_ADDRESS as `0x${string}`,
  abi: parseAbi([
    'function totalChildContracts(uint256 tokenId) external view returns (uint256)',
    'function childContractByIndex(uint256 tokenId, uint256 index) external view returns (address childContract)',
    'function totalChildTokens(uint256 tokenId, address childContract) external view returns (uint256)',
    'function childTokenByIndex(uint256 tokenId, address childContract, uint256 index) external view returns (uint256 childTokenId)',
    'function safeTransferChild(uint256 fromTokenId, address to, address childContract, uint256 childTokenId) external',
  ]),
}

interface MomentSummary {
  id: number
  moment: number
}
// Assemble a listing of all known Moment token IDs, and the associated Moment group they belong to
let allMoments: MomentSummary[] = []
for (let m of moments) {
  for (let i = 0; i < m.issuance; i++) {
    allMoments.push({
      id: i + m.startingTokenId,
      moment: m.momentId,
    })
  }
}

interface TokenMeta {
  collectionAddress: `0x${string}`
  collectionName: React.ReactNode
  tokenId: bigint
  imageSrc: string
  link?: string
}

/**
 * Given a token contract address and token ID, decide what human-friendly label, image, and link to use for that token
 */
const getTokenMeta = (collectionAddress: `0x${string}`, tokenId: bigint): TokenMeta => {
  // Use collection address to determine if we know what sort of token that child token is
  switch (collectionAddress) {
    case ACCLIMATOR_ADDRESS:
      // Child token is a MoonCat
      return {
        collectionAddress,
        collectionName: 'MoonCat',
        tokenId,
        imageSrc: `${API_SERVER_ROOT}/image/${tokenId}?scale=3&padding=5`,
        link: `/mooncats/${tokenId}`,
      }
    case MOMENTS_ADDRESS:
      // Child token is a MoonCatMoment
      let meta = allMoments.find((m) => m.id == Number(tokenId))
      return {
        collectionAddress,
        collectionName: `MoonCat${ZWS}Moment`,
        tokenId,
        imageSrc: moments[meta!.moment].meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/'),
        link: `/moments/${meta!.moment}`,
      }
    default:
      // Child token is an unknown token
      // Show the collection address and token ID, and use a random anonymous image for this child token
      const hash = sha256((collectionAddress + BigInt(tokenId).toString(16)) as `0x${string}`, 'bytes')
      // Image is deterministic, based on collection and token IDs
      const num = (hash[0] % 3) + 1
      return {
        collectionAddress,
        collectionName: <code>{collectionAddress.substring(0, 10)}&hellip;</code>,
        tokenId,
        imageSrc: `/img/p${num}.png`,
      }
  }
}

/**
 * Widget for donating an ERC721 token to a MoonCat
 */
const SendToMoonCat = ({ moonCat, owner }: { moonCat: MoonCatData; owner: `0x${string}` | undefined }) => {
  const [startProcess, setStartProcess] = useState<boolean>(false)
  const { open } = useWeb3Modal()
  const { isConnected, isSignedIn, connectedAddress, verifiedAddresses, status, doSignIn } = useSignedIn()
  const { viewMessage, processTransaction } = useTxStatus()
  const router = useRouter()

  const [fetchStatus, setFetchStatus] = useFetchStatus()
  const [ownedTokens, setOwnedTokens] = useState<TokenMeta[]>([])
  const [tokenChoice, setTokenChoice] = useState<TokenMeta | false>(false)

  // Fetch tokens owned by the current visitor
  useEffect(() => {
    if (!startProcess || typeof connectedAddress == 'undefined') return
    let ignore = false
    async function doWork() {
      setFetchStatus('pending')
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${connectedAddress}`)
      if (!rs.ok) {
        console.error('Failed to fetch owner profile', rs)
        setFetchStatus('error')
        return
      }
      if (ignore) return

      const data = await rs.json()
      if (ignore) return

      let tokens: TokenMeta[] = (data.ownedMoonCats as OwnedMoonCat[])
        .map((mc) => getTokenMeta(ACCLIMATOR_ADDRESS, BigInt(mc.rescueOrder)))
        .concat(
          (data.ownedMoments as { moonCat?: number; momentId: number }[])
            .filter((m) => typeof m.moonCat == 'undefined')
            .map((m) => getTokenMeta(MOMENTS_ADDRESS, BigInt(m.momentId)))
        )
      setOwnedTokens(tokens)
      setFetchStatus('done')
    }
    doWork()

    return () => {
      ignore = true
    }
  }, [startProcess, connectedAddress, setFetchStatus])

  // Send the selected token to the current MoonCat
  async function sendToken() {
    if (!tokenChoice) return

    function numToBytes32(num: number) {
      let hex = num.toString(16)
      while (hex.length < 64) {
        hex = '0' + hex
      }
      return '0x' + hex
    }

    let rs = await processTransaction({
      address: tokenChoice.collectionAddress,
      abi: parseAbi(['function safeTransferFrom(address from, address to, uint256 tokenId, bytes data) external']),
      functionName: 'safeTransferFrom',
      args: [connectedAddress, ACCLIMATOR_ADDRESS, tokenChoice.tokenId, numToBytes32(moonCat.rescueOrder)],
    })
    if (rs) {
      router.reload()
    }
  }

  const preamble = (
    <p>
      <strong>Gift a token to this MoonCat:</strong> any ERC721 token can be gifted to a MoonCat. Once gifted, only the
      owner of that MoonCat can transfer that token elsewhere.
    </p>
  )

  // To interact with the MoonCat's Purrse, the visitor must be connected and signed-in
  if (!isConnected) {
    // If the visitor isn't connected, they need to make a connection first. After that they can sign in
    return (
      <section className="card">
        {preamble}
        <p>
          <button
            onClick={(e) => {
              open()
            }}
          >
            Connect
          </button>
        </p>
      </section>
    )
  } else if (!isSignedIn) {
    // User has not signed-in yet
    let statusView: React.ReactNode
    if (status == 'pending') {
      statusView = <LoadingIndicator message="Signing in..." />
    } else if (status == 'error') {
      statusView = <WarningIndicator message="Error encountered logging in" />
    }
    return (
      <section className="card">
        {preamble}
        <p>
          <button
            onClick={(e) => {
              doSignIn().then(() => {
                setStartProcess(true) // The user interacted; kick off owned token enumeration after they're connected
              })
            }}
          >
            Sign In
          </button>
        </p>
        {statusView}
      </section>
    )
  } else if (!startProcess) {
    // The visitor is connected and signed-in, but hasn't given indication they want to give a gift
    // Wait for them to click a button to signal that intent before starting owned token enumeration
    const label = owner && verifiedAddresses.includes(owner) ? 'Manage' : 'Find Tokens to Gift'
    return (
      <section className="card">
        {preamble}
        <p>
          <button onClick={(e) => setStartProcess(true)}>{label}</button>
        </p>
      </section>
    )
  }

  // If the visitor is connected and signed in, and the async process to enumerate their balances kicked off. Is it in progress?
  if (fetchStatus == 'start' || fetchStatus == 'pending') {
    return (
      <section className="card">
        {preamble}
        <LoadingIndicator message="Finding eligible tokens" />
      </section>
    )
  }

  // The async process has finished; enumerate which tokens the visitor owns
  // If the visitor doesn't have any tokens from known collections, show error message
  if (ownedTokens.length == 0) {
    return (
      <section className="card">
        {preamble}
        <p>No tokens found in your address that could be gifted at this time.</p>
      </section>
    )
  }

  // The visitor has tokens that are possible to be gifted to a MoonCat; show widget for doing so
  const visitorIsOwner = owner && verifiedAddresses.includes(owner)
  const giftButton =
    tokenChoice === false ? (
      <section className="sub-card">
        <p>Click a token you wish to send to this MoonCat</p>
      </section>
    ) : (
      <section className="sub-card">
        <p>
          <picture>
            <img
              style={{ maxHeight: '100px' }}
              src={tokenChoice.imageSrc}
              alt={`${tokenChoice.collectionName} #${tokenChoice.tokenId}`}
            />
          </picture>
        </p>
        <p>
          Send {tokenChoice.collectionName} #{tokenChoice.tokenId.toString()} to MoonCat #{moonCat.rescueOrder}:
        </p>
        <p>
          <button onClick={sendToken}>{visitorIsOwner ? 'Send' : 'Gift'}</button>
        </p>
        {viewMessage}
      </section>
    )
  return (
    <section className="card">
      {preamble}
      <div style={{ columnWidth: '15em', marginBottom: '2rem', fontSize: '0.8rem' }}>
        <table cellSpacing="0" cellPadding="0" className="zebra" style={{ width: '100%' }}>
          <tbody>
            {ownedTokens.map((t) => {
              const key = t.collectionAddress + '::' + t.tokenId
              let classes = []
              if (
                tokenChoice &&
                t.collectionAddress == tokenChoice.collectionAddress &&
                t.tokenId == tokenChoice.tokenId
              ) {
                classes.push('highlight')
              }

              return (
                <tr
                  key={key}
                  style={{ cursor: 'pointer' }}
                  className={classes.join(' ')}
                  onClick={(e) => setTokenChoice(t)}
                >
                  <td style={{ textAlign: 'center', padding: '0.2rem' }}>
                    <picture>
                      <img
                        style={{ maxHeight: '1.5em', verticalAlign: 'middle' }}
                        src={t.imageSrc}
                        alt={`${t.collectionName} #${t.tokenId}`}
                      />
                    </picture>
                  </td>
                  <td>
                    {t.collectionName} #{t.tokenId.toString()}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
      {giftButton}
    </section>
  )
}

const MoonCatTabPurrse = ({ moonCat, details }: TabProps) => {
  const [fetchStatus, setStatus] = useFetchStatus()
  const [childTokens, setChildTokens] = useState<TokenMeta[]>([])
  const { connectedAddress, isSignedIn, verifiedAddresses } = useSignedIn()
  const { viewMessage, processTransaction } = useTxStatus()
  const router = useRouter()

  useEffect(() => {
    let ignore = false

    async function doWork() {
      setStatus('pending')
      if (!(await switchToChain(1))) {
        console.error('Failed to switch networks')
        setStatus('error')
        return
      }

      // Start with fetching how many collections this MoonCat has in their Purrse
      const collectionCount = await readContract({
        ...ACCLIMATOR,
        functionName: 'totalChildContracts',
        args: [BigInt(moonCat.rescueOrder)],
      })
      if (ignore) return
      if (collectionCount == 0n) {
        setChildTokens([])
        setStatus('done')
        return
      }

      // Iterate up to that collection count to fetch which contracts they are
      let multicalls = []
      for (let i = 0n; i < collectionCount; i++) {
        multicalls.push({
          ...ACCLIMATOR,
          functionName: 'childContractByIndex',
          args: [BigInt(moonCat.rescueOrder), i],
        } as const)
      }
      const collectionAddresses = await multicall({ contracts: multicalls, allowFailure: false })
      if (ignore) return

      // For each collection, find out how many tokens from that collection
      multicalls = []
      for (let collectionAddress of collectionAddresses) {
        multicalls.push({
          ...ACCLIMATOR,
          functionName: 'totalChildTokens',
          args: [BigInt(moonCat.rescueOrder), collectionAddress],
        } as const)
      }
      const collectionCounts = await multicall({ contracts: multicalls, allowFailure: false })
      if (ignore) return

      // Next, change the child index values into token IDs
      let inputIndex = []
      multicalls = []
      for (let i = 0; i < collectionAddresses.length; i++) {
        const collectionAddress = collectionAddresses[i]
        const childCount = collectionCounts[i]
        for (let i = 0n; i < childCount; i++) {
          inputIndex.push(collectionAddress)
          multicalls.push({
            ...ACCLIMATOR,
            functionName: 'childTokenByIndex',
            args: [BigInt(moonCat.rescueOrder), collectionAddress, i],
          } as const)
        }
      }
      const childIds = await multicall({ contracts: multicalls, allowFailure: false })
      if (ignore) return

      // Merge the data together
      let data: TokenMeta[] = []
      for (let i = 0; i < inputIndex.length; i++) {
        data.push(getTokenMeta(inputIndex[i] as `0x${string}`, BigInt(childIds[i])))
      }
      // Sort the tokens to put them in ascending order, by collection
      data = data.sort((a, b) => {
        if (a.collectionAddress != b.collectionAddress) {
          return a.collectionAddress.localeCompare(b.collectionAddress)
        } else {
          return Number(a.tokenId - b.tokenId)
        }
      })
      setChildTokens(data)
      setStatus('done')
    }
    doWork()
    return () => {
      ignore = true
    }
  }, [moonCat.rescueOrder, setStatus])

  // Withdraw a token from the MoonCat, to the owner's wallet
  async function withdrawToken(childToken: TokenMeta) {
    let rs = await processTransaction({
      ...ACCLIMATOR,
      functionName: 'safeTransferChild',
      args: [moonCat.rescueOrder, connectedAddress, childToken.collectionAddress, childToken.tokenId],
    })
    if (rs) {
      router.reload()
    }
  }

  let bodyView: React.ReactNode
  if (fetchStatus == 'start' || fetchStatus == 'pending') {
    bodyView = <LoadingIndicator style={{ marginBottom: '1rem' }} />
  } else if (childTokens.length == 0) {
    bodyView = (
      <section className="card">
        <p>This MoonCat doesn&rsquo;t have anything in its Purrse right now.</p>
      </section>
    )
  } else {
    // Assemble a grid to display child tokens
    bodyView = (
      <div id="item-grid" style={{ marginBottom: '1rem' }}>
        {childTokens.map((child) => {
          const key = child.collectionAddress + '::' + child.tokenId
          let sendLink: React.ReactNode
          if (isSignedIn && typeof details.owner != 'undefined' && verifiedAddresses.includes(details.owner)) {
            sendLink = (
              <button
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  margin: '0.2rem',
                  padding: '0.2rem',
                  borderWidth: 0,
                  lineHeight: 0,
                  fontSize: '0.8rem',
                }}
                title="Send"
                onClick={(e) => {
                  e.preventDefault()
                  withdrawToken(child)
                }}
              >
                <Icon name="rocket" />
              </button>
            )
          }
          if (!child.link) {
            return (
              <div key={key} className="item-thumb" style={{ position: 'relative' }}>
                {sendLink}
                <div className="thumb-img" style={{ backgroundImage: `url(${child.imageSrc})` }} />
                <p>{child.collectionName}</p>
                <p>#{child.tokenId.toString()}</p>
              </div>
            )
          } else {
            return (
              <a key={key} href={child.link} className="item-thumb" style={{ position: 'relative' }}>
                {sendLink}
                <div className="thumb-img" style={{ backgroundImage: `url(${child.imageSrc})` }} />
                <p>{child.collectionName}</p>
                <p>#{child.tokenId.toString()}</p>
              </a>
            )
          }
        })}
      </div>
    )
  }

  return (
    <div className="text-container">
      <section className="card-help">
        <p>
          MoonCats are able to contain/own other NFTs (following the{' '}
          <a href="https://eips.ethereum.org/EIPS/eip-998" target="_blank" rel="noreferrer">
            ERC998 standard
          </a>
          ), which then stay with the MoonCat even if the MoonCat is moved to a different wallet. Any ERC721-compliant
          token can be given to a MoonCat (put in their Purrse).
        </p>
        {!details.isAcclimated && (
          <p>
            MoonCats need to be Acclimated in order to interact with this feature, and this MoonCat is currently not
            Acclimated. Any tokens previously added to the MoonCat&rsquo;s Purrse will wait for them to be Acclimated
            again.
          </p>
        )}
      </section>
      {bodyView}
      {viewMessage}
      {details.isAcclimated && <SendToMoonCat moonCat={moonCat} owner={details.owner} />}
    </div>
  )
}
export default MoonCatTabPurrse
