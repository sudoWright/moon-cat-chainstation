import { API_SERVER_ROOT, IPFS_GATEWAY, useFetchStatus } from 'lib/util'
import React, { useEffect, useState } from 'react'
import { Address } from 'viem'
import LoadingIndicator from './LoadingIndicator'
import WarningIndicator from './WarningIndicator'
import { Treasure } from 'lib/firebase'
import Icon from './Icon'
import { Moment } from 'lib/types'
const moments: Moment[] = require('lib/moments_meta.json')

type TreasureRef =
  | {
      treasure: Treasure
      moonCats: string[]
    }
  | {
      treasure: Treasure
      moments: number[]
    }

interface Props {
  address?: Address
}

const TreasuresList = ({ address }: Props) => {
  const [status, setStatus] = useFetchStatus()
  const [treasures, setTreasures] = useState<TreasureRef[]>([])

  useEffect(() => {
    let ignore = false
    const targetUrl = typeof address == 'undefined' ? '/api/treasures' : '/api/treasures?address=' + address
    setStatus('pending')
    fetch(targetUrl)
      .then((rs) => {
        if (!rs.ok) {
          console.error('Failed to fetch', rs)
          setStatus('error')
          return
        }
        return rs.json()
      })
      .then((data) => {
        if (ignore) return
        setTreasures(data.sort((a: TreasureRef, b: TreasureRef) => a.treasure.label.localeCompare(b.treasure.label)))
        setStatus('done')
      })

    return () => {
      ignore = true
    }
  }, [address, setStatus])

  if (status == 'start' || status == 'pending') {
    return <LoadingIndicator message="Digging up some Treasures for you..." />
  } else if (status == 'error') {
    return <WarningIndicator message="Data fetch encountered an error" />
  } else if (treasures.length == 0) {
    return (
      <section className="card-notice">
        Based on your current address holdings, you don&rsquo;t qualify for any Treasures. Keep intereacting with the
        MoonCatRescue ecosystem and something will likely pop up here!
      </section>
    )
  }

  const maxMoonCatPreviews = 30

  return (
    <>
      <section className="card-help">
        <p>
          You are granted access to the following files, based on the assets you currently own (the asset(s) you own
          that grant access to that specific Treasure are shown to the right of that Treasure). The license you are
          granted to these Treasure assets (for as long as you own a MoonCatRescue asset that unlocks that Treasure) is:
        </p>
        <div className="callout" style={{ fontSize: '0.8rem', lineHeight: '1.1em' }}>
          <p>
            <strong>You may</strong> use the assets linked in this section for your personal enjoyment and private use
            (e.g. you may print them out and display in your private space, or use as wallpaper on your personal
            devices).
          </p>
          <p>
            <strong>You may NOT</strong> redistribute assets linked in this section to others.{' '}
            <strong>You may NOT</strong> use the assets linked in this section for commercial purposes.
          </p>
          <p>
            If the linked assets contains a folder named &ldquo;public&rdquo;, <strong>you may</strong> share the
            contents of that folder publicly (this will often be a badge or thumbnail image that you can share freely
            with others to give a taste of what that Treasure is). <strong>You may</strong> use the name and description
            of the Treasure publicly.
          </p>
        </div>
        <p>
          Each individual Treasure may have other licensing terms (look for a &ldquo;license&rdquo; file in the linked
          asset folder).
        </p>
      </section>
      <dl className="treasures-list">
        {treasures.map((t, i) => {
          let iconLink = t.treasure.icon?.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
          if (t.treasure.type == 'search') {
            // Treasure is a 'search' type that might apply to multiple this address holds
            if ('moonCats' in t) {
              // Treasure is for MoonCats
              const catStyle = t.moonCats.length >= 10 ? { height: 20 } : { height: 40 }
              let shownMoonCats = t.moonCats.slice(0, maxMoonCatPreviews).map((catId) => {
                return (
                  <picture key={catId}>
                    <img
                      src={`${API_SERVER_ROOT}/image/${catId}?scale=2&padding=3`}
                      alt={`MoonCat ${catId}`}
                      title={`MoonCat ${catId}`}
                      style={catStyle}
                    />
                  </picture>
                )
              })
              return (
                <React.Fragment key={i}>
                  <dt>{t.treasure.label}</dt>
                  <dd>
                    {iconLink && (
                      <picture>
                        <img src={iconLink} alt="" style={{ height: 60, width: 'auto' }} />
                      </picture>
                    )}
                    <div className="details">
                      {t.treasure.details}
                      <br />
                      <a href={IPFS_GATEWAY + '/ipfs/' + t.treasure.ipfs} target="_blank" rel="noreferrer">
                        Get Treasure
                        <Icon name="link-external" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
                      </a>
                    </div>
                    <div className="eligible-mooncats">
                      {shownMoonCats}
                      {t.moonCats.length > maxMoonCatPreviews && '...'}
                    </div>
                  </dd>
                </React.Fragment>
              )
            } else {
              // Treasure is for Moments
            }
          } else if (t.treasure.type == 'mapped') {
            // Treasure is a 'mapped' type
            if ('moonCat' in t.treasure.mapping) {
              // Treasure is for MoonCats
              let shownMoonCats: React.ReactNode[] = Object.entries(t.treasure.mapping.moonCat).map((e) => {
                return (
                  <li key={e[0]} style={{ marginBottom: '1rem' }}>
                    <a href={IPFS_GATEWAY + '/ipfs/' + e[1]} target="_blank" rel="noreferrer">
                      <picture>
                        <img
                          src={`${API_SERVER_ROOT}/image/${e[0]}?scale=2&padding=3`}
                          alt={`MoonCat ${e[0]}`}
                          title={`MoonCat ${e[0]}`}
                          style={{ height: 40, verticalAlign: -12, paddingRight: '1rem' }}
                        />
                      </picture>
                      Treasure for MoonCat {e[0]}
                      <Icon name="link-external" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
                    </a>
                  </li>
                )
              })

              return (
                <React.Fragment key={i}>
                  <dt>{t.treasure.label}</dt>
                  <dd>
                    {iconLink && (
                      <picture>
                        <img src={iconLink} alt="" style={{ height: 60, width: 'auto' }} />
                      </picture>
                    )}
                    <div className="details">
                      {t.treasure.details}
                      <ul style={{ listStyleType: 'none', paddingLeft: 0 }}>{shownMoonCats}</ul>
                    </div>
                  </dd>
                </React.Fragment>
              )
            } else {
              // Treasure is for Moments
              let shownMoments: React.ReactNode[] = Object.entries(t.treasure.mapping.moment).map((e) => {
                const imgSrc = moments[Number(e[0])].meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
                return (
                  <li key={e[0]} style={{ marginBottom: '1rem' }}>
                    <a href={IPFS_GATEWAY + '/ipfs/' + e[1]} target="_blank" rel="noreferrer">
                      <picture>
                        <img
                          src={imgSrc}
                          alt={`Moment ${e[0]}`}
                          style={{ height: 50, verticalAlign: -16, paddingRight: '1rem' }}
                        />
                      </picture>
                      Treasure for &ldquo;{moments[Number(e[0])].meta.name}&rdquo;
                      <Icon name="link-external" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
                    </a>
                  </li>
                )
              })
              return (
                <React.Fragment key={i}>
                  <dt>{t.treasure.label}</dt>
                  <dd>
                    {iconLink && (
                      <picture>
                        <img src={iconLink} alt="" style={{ height: 60, width: 'auto' }} />
                      </picture>
                    )}
                    <div className="details">
                      {t.treasure.details}
                      <ul style={{ listStyleType: 'none', paddingLeft: 0 }}>{shownMoments}</ul>
                    </div>
                  </dd>
                </React.Fragment>
              )
            }
          } else {
            // Treasure of unknown type?
            console.error('Unknown Treasure type', t)
          }
        })}
      </dl>
    </>
  )
}

export default TreasuresList
