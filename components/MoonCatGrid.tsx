import React, { useCallback, useEffect, useState } from 'react'
import MoonCatThumb from './MoonCatThumb'
import MoonCatFilters from './MoonCatFilters'
import MoonCatViewSelector from './MoonCatViewSelector'
import Pagination from './Pagination'
import { MoonCatData, MoonCatFilterSettings } from '../lib/types'
import { useFetchStatus } from '../lib/util'
import { useRouter } from 'next/router'

const DEFAULT_PER_PAGE = 50

interface FilterParams {
  pageNumber: number
  perPage: number
  moonCats: number[] | 'all'
  filters: MoonCatFilterSettings
}

interface PageState {
  moonCatPage: MoonCatData[]
  filteredSetSize: number
  totalMoonCats: number
}

interface Props {
  children?: Parameters<typeof MoonCatThumb>[0]['thumbHandler']
  moonCats: number[] | 'all'
  perPage?: number
  apiPage: string
  isEventActive?: boolean
}

const MoonCatGrid = ({
  children: thumbHandler,
  moonCats: allMoonCats,
  perPage,
  apiPage,
  isEventActive = false,
}: Props) => {
  const router = useRouter()

  if (!perPage) {
    perPage = DEFAULT_PER_PAGE
  }

  let initialPage = 0
  // Parse the URL parameters to see if we should jump to a specific page
  const pagePreference = router.query['page']
  if (typeof pagePreference != 'undefined' && !Array.isArray(pagePreference)) {
    let page = parseInt(pagePreference)
    if (page > 0) {
      // Page is valid; jump to it
      console.debug('Jumping to page:', page)
      initialPage = page - 1
    }
  }

  const [filterProps, setFilterProps] = useState<FilterParams>({
    pageNumber: initialPage,
    perPage: perPage,
    moonCats: allMoonCats,
    filters: {},
  })

  const [status, setStatus] = useFetchStatus()
  const [{ moonCatPage, filteredSetSize, totalMoonCats }, setPageState] = useState<PageState>({
    moonCatPage: [],
    filteredSetSize: 0,
    totalMoonCats: -1,
  })

  // Call the local API endpoint to fetch a page of results
  const doFilter = useCallback(
    async function ({ pageNumber, perPage, moonCats, filters }: FilterParams) {
      setStatus('pending')
      let params = new URLSearchParams(filters as Record<string, string>)
      let fetchMode = 'GET'
      params.set('limit', String(perPage!))
      params.set('offset', String(pageNumber * perPage!))
      if (Array.isArray(moonCats)) {
        params.set('mooncats', moonCats.join(','))
        fetchMode = 'POST'
      } else {
        params.set('mooncats', moonCats)
      }
      try {
        let searchRes =
          fetchMode == 'GET'
            ? await fetch(`/api/${apiPage}?${params.toString()}`)
            : await fetch(`/api/${apiPage}`, {
                method: fetchMode,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(Object.fromEntries(params))
              })
        let data = await searchRes.json()
        setPageState({
          moonCatPage: data.moonCats,
          filteredSetSize: data.length,
          totalMoonCats: data.totalLength,
        })
        setStatus('done')
      } catch (err) {
        console.error(err)
        setStatus('error')
      }
    },
    [apiPage, setStatus]
  )

  // Whenever the filter choices change, fetch an updated filtered set of data from the back-end
  useEffect(() => {
    doFilter(filterProps)
  }, [doFilter, filterProps])

  // Event handler for updates from MoonCatFilter component when a user picks a new filter value
  function handleFilterUpdate(prop: keyof MoonCatFilterSettings, newValue: any) {
    setFilterProps((curProps) => {
      let newFilters = Object.assign({}, curProps.filters)
      if (newValue == '') {
        if (!curProps.filters[prop]) {
          // Already blank
          return curProps
        }
        delete newFilters[prop]
      } else {
        if (curProps.filters[prop] == newValue) {
          // Already set to that value
          return curProps
        }
        newFilters[prop] = newValue
      }

      return { ...curProps, filters: newFilters, pageNumber: 0 }
    })
  }

  // Event handler for updates from Pagination component when user navigates to a new page
  function handlePageUpdate(newPage: number) {
    setFilterProps((curProps) => {
      if (curProps.pageNumber == newPage) {
        // Already set to that value
        return curProps
      }

      // Update the query parameter in the URL.
      router.replace(
        {
          query: {
            ...router.query,
            page: String(newPage + 1),
          },
        },
        undefined,
        { scroll: false }
      )
      return { ...curProps, pageNumber: newPage }
    })
  }

  return (
    <div className="mooncat-grid">
      <div
        style={{
          display: 'flex',
          alignItems: 'baseline',
          padding: '1rem 0',
        }}
        className="text-scrim"
      >
        {totalMoonCats > 1 && (
          <MoonCatFilters
            id="filters"
            currentFilters={filterProps.filters}
            filteredCount={filteredSetSize}
            totalCount={totalMoonCats}
            onChange={handleFilterUpdate}
          />
        )}
        <div style={{ flexGrow: 10 }} />
        <MoonCatViewSelector isEventActive={isEventActive} />
      </div>
      <div id="item-grid" style={{ margin: '0 0 2rem' }}>
        {moonCatPage.map((mc) => (
          <MoonCatThumb key={mc.rescueOrder} moonCat={mc} thumbHandler={thumbHandler} />
        ))}
      </div>
      <Pagination
        currentPage={filterProps.pageNumber}
        maxPage={Math.ceil(filteredSetSize / perPage) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </div>
  )
}
export default MoonCatGrid
