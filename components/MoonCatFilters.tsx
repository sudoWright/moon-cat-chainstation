import React, { useCallback, useEffect, useState } from 'react'
import { MoonCatFilterSettings, SelectFieldMeta, TextFieldMeta, isSelectFieldMeta } from 'lib/types'
import { interleave } from 'lib/util'
import { SelectField, TextField } from './FormFields'

const filterMeta: Array<SelectFieldMeta | TextFieldMeta> = [
  {
    name: 'classification',
    type: 'select',
    label: 'Classification',
    defaultLabel: 'Any',
    options: {
      genesis: 'Genesis',
      rescue: 'Rescue',
    },
  } as SelectFieldMeta,
  {
    name: 'hue',
    type: 'select',
    label: 'Hue',
    defaultLabel: 'Any',
    options: {
      white: 'White',
      black: 'Black',
      red: 'Red',
      orange: 'Orange',
      yellow: 'Yellow',
      chartreuse: 'Chartreuse',
      green: 'Green',
      teal: 'Teal',
      cyan: 'Cyan',
      skyblue: 'SkyBlue',
      blue: 'Blue',
      purple: 'Purple',
      magenta: 'Magenta',
      fuchsia: 'Fuchsia',
    },
  } as SelectFieldMeta,
  {
    name: 'pale',
    type: 'select',
    label: 'Pale-Colored',
    defaultLabel: 'Any',
    options: {
      no: 'No',
      yes: 'Yes',
    },
  } as SelectFieldMeta,
  {
    name: 'facing',
    type: 'select',
    label: 'Facing',
    defaultLabel: 'Any',
    options: {
      right: 'Right',
      left: 'Left',
    },
  } as SelectFieldMeta,
  {
    name: 'expression',
    type: 'select',
    label: 'Expression',
    defaultLabel: 'Any',
    options: {
      smiling: 'Smiling',
      grumpy: 'Grumpy',
      pouting: 'Pouting',
      shy: 'Shy',
    },
  } as SelectFieldMeta,
  {
    name: 'pattern',
    type: 'select',
    label: 'Coat Pattern',
    defaultLabel: 'Any',
    options: {
      pure: 'Pure',
      tabby: 'Tabby',
      spotted: 'Spotted',
      tortie: 'Tortie',
    },
  } as SelectFieldMeta,
  {
    name: 'pose',
    type: 'select',
    label: 'Pose',
    defaultLabel: 'Any',
    options: {
      standing: 'Standing',
      pouncing: 'Pouncing',
      stalking: 'Stalking',
      sleeping: 'Sleeping',
    },
  } as SelectFieldMeta,
  {
    name: 'rescueYear',
    type: 'select',
    label: 'Rescue Year',
    defaultLabel: 'Any',
    options: {
      2017: '2017',
      2018: '2018',
      2019: '2019',
      2020: '2020',
      2021: '2021',
    },
  } as SelectFieldMeta,
  {
    name: 'named',
    type: 'select',
    label: 'Named',
    defaultLabel: 'Any',
    options: {
      no: 'Not Named',
      yes: 'Named',
      valid: 'String Names',
      invalid: 'Other Names',
    },
  } as SelectFieldMeta,
  { name: 'nameKeyword', label: 'Name Keyword' } as TextFieldMeta,
]

interface Props {
  id?: string
  currentFilters: MoonCatFilterSettings
  filteredCount: number
  totalCount: number
  onChange: (propName: keyof MoonCatFilterSettings, newValue: any) => void
}
const MoonCatFilters = ({ id, currentFilters, filteredCount, totalCount, onChange }: Props) => {
  const [isOpen, setOpen] = useState<boolean>(false)

  /**
   * Handle a change to one of the form elements
   */
  function handleChange(e: any) {
    const propName = e.target.name as keyof MoonCatFilterSettings
    const newValue = e.target.value
    onChange(propName, newValue)
  }

  /**
   * Set the visibility of the full filters form to the opposite of what it currently is
   */
  function toggleModal() {
    setOpen(!isOpen)
  }

  /**
   * Document-level event listener
   * For every key press, if it was the Escape key, close the filters form
   */
  const escListener = useCallback(
    (event: any) => {
      if (event.key == 'Escape') {
        setOpen(false)
      }
    },
    [setOpen]
  )

  /**
   * On mount, start listening for keypresses
   */
  useEffect(() => {
    document.addEventListener('keydown', escListener, false)
    return () => {
      document.removeEventListener('keydown', escListener, false)
    }
  }, [escListener])

  // Build UI labels for the current filter state
  let filterPhrases = []
  for (let [propName, currentValue] of Object.entries(currentFilters)) {
    const filterName = propName as keyof MoonCatFilterSettings
    const meta = filterMeta.find((m) => m.name == filterName)
    if (typeof meta == 'undefined') {
      console.error('Failed to find form meta for', filterName)
    }
    if (typeof meta !== 'undefined' && !!currentValue) {
      const displayValue = isSelectFieldMeta(meta) ? (meta as SelectFieldMeta).options[currentValue] : currentValue
      filterPhrases.push(
        <React.Fragment key={filterName}>
          {meta.label}: <em>{displayValue}</em>
        </React.Fragment>
      )
    }
  }
  let filterPhrase: Array<JSX.Element | string> = ['']
  if (filterPhrases.length > 0) {
    filterPhrase = interleave(filterPhrases, ', ')
    filterPhrase.push('. ')
  }
  let filterSize: React.ReactNode
  if (filteredCount < totalCount) {
    filterSize = `${filteredCount.toLocaleString()} of ${totalCount.toLocaleString()} MoonCats`
  } else if (totalCount == 1) {
    // Showing just one MoonCat? No label for this
  } else if (totalCount == 2) {
    filterSize = `Showing both MoonCats`
  } else if (totalCount > 0) {
    filterSize = `Showing all ${totalCount.toLocaleString()} MoonCats`
  }

  const modalStyle = isOpen ? {} : { display: 'none' }
  const labelStyle = { width: '200px' }
  return (
    <div id={id} style={{ position: 'relative', display: 'flex', alignItems: 'baseline' }}>
      <button onClick={toggleModal}>Filter</button>
      <div style={{ padding: '0 1rem', fontSize: '0.8rem' }}>
        {filterPhrase}
        {filterSize}
      </div>
      <div className="filter-modal" style={modalStyle}>
        {filterMeta.map((m) => {
          if (isSelectFieldMeta(m)) {
            return (
              <SelectField
                key={m.name}
                meta={m}
                labelStyle={labelStyle}
                currentValue={currentFilters.classification}
                onChange={handleChange}
              />
            )
          } else {
            return (
              <TextField
                key={m.name}
                meta={m}
                labelStyle={labelStyle}
                currentValue={currentFilters.nameKeyword}
                onChange={handleChange}
              />
            )
          }
        })}
      </div>
    </div>
  )
}
export default MoonCatFilters
