import React, { ReactNode, useEffect, useState } from 'react'
import {
  ADDRESS_DETAILS,
  API2_SERVER_ROOT,
  IPFS_GATEWAY,
  MOMENTS_ADDRESS,
  formatTimestamp,
  useFetchStatus,
} from 'lib/util'
import { TabProps } from '../pages/mooncats/[id]'
import { Address } from 'viem'
import LoadingIndicator from './LoadingIndicator'
import EthereumAddress from './EthereumAddress'
import EthereumTransaction from './EthereumTransaction'
import { Moment } from 'lib/types'
import Link from 'next/link'
const moments: Moment[] = require('lib/moments_meta.json')

const RESCUE_ADDRESS = '0x60cd862c9C687A9dE49aecdC3A99b74A4fc54aB6'
const ACCLIMATOR_ADDRESS = '0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69'
const JUMPPORT_ADDRESS = '0xF4d150F3D03Fa1912aad168050694f0fA0e44532'
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

interface BlockchainEvent {
  args: Record<string, any>
  eventContract: `0x${string}`
  logIndex: number
  tx: {
    data: `0x${string}`
    from: Address
    to: Address
    value: string
    hash: `0x${string}`
  }
  blockNumber: number
  eventName: string
  timestamp: number
}

/**
 * The timeline view is mostly Events, but has some Labels between some events, to provide logical groupings between them
 */
interface TimelineEventNode {
  type: 'event'
  data: BlockchainEvent
}
interface TimelineLabelNode {
  type: 'label'
  data: string
}

/**
 * Reformat Moment minting data into the shape needed for timeline display
 */
const MOMENT_RELEASES: BlockchainEvent[] = moments.map((m) => ({
  args: {
    momentId: m.momentId,
    URI: m.tokenURI,
    rescueOrders: m.moonCats,
  },
  eventContract: MOMENTS_ADDRESS,
  logIndex: 0,
  tx: {
    data: '0xac8ad13e',
    from: m.tx.from,
    to: m.tx.to,
    value: '0',
    hash: m.tx.hash,
  },
  blockNumber: m.tx.blockNumber,
  eventName: 'MomentMint',
  timestamp: m.tx.timestamp,
}))

/**
 * For a raw Blockhain Event, parse out a more human-friendly description of what it represents
 */
const getMessageForEvent = function (event: BlockchainEvent): {
  title: string
  description: ReactNode
  image?: string
  dotClass: string
} {
  // Transfer event from Acclimator contract
  if (event.eventName == 'Transfer' && event.eventContract == ACCLIMATOR_ADDRESS) {
    if (ADDRESS_DETAILS[event.args.to] && ADDRESS_DETAILS[event.args.to].type == 'pool') {
      // The address the transfer is going to is a known NFT pool address
      return {
        title: 'Put up for adoption',
        description: (
          <p>
            Moved to <EthereumAddress address={event.args.to} /> to seek a new owner. Thanks for the time,{' '}
            <EthereumAddress address={event.args.from} />!
          </p>
        ),
        dotClass: 'dot',
      }
    }
    if (ADDRESS_DETAILS[event.args.from] && ADDRESS_DETAILS[event.args.from].type == 'pool') {
      // The address the transfer came from is a known NFT pool address
      return {
        title: 'Adopted by new owner',
        description: (
          <p>
            Got adopted by <EthereumAddress address={event.args.to} /> from{' '}
            <EthereumAddress address={event.args.from} />!
          </p>
        ),
        dotClass: 'dot',
      }
    }
    if (ADDRESS_DETAILS[event.args.to] && ADDRESS_DETAILS[event.args.to].type == 'bridge') {
      // The address the transfer is going to is a known NFT bridge address
      return {
        title: 'Went on an adventure',
        description: (
          <p>
            Took a trip across <EthereumAddress address={event.args.to} /> to have some adventures elsewhere.
          </p>
        ),
        dotClass: 'dot',
      }
    }
    if (ADDRESS_DETAILS[event.args.from] && ADDRESS_DETAILS[event.args.from].type == 'bridge') {
      // The address the transfer came from is a known NFT bridge address
      return {
        title: 'Returned from an adventure',
        description: (
          <p>
            Came back across <EthereumAddress address={event.args.from} /> to{' '}
            <EthereumAddress address={event.args.to} />, for further adventures here on ChainStation Alpha.
          </p>
        ),
        dotClass: 'dot',
      }
    }

    // No additional details are known for this transfer; treat it as an adoption
    return {
      title: 'Adopted by new owner',
      description: (
        <p>
          Got adopted by <EthereumAddress address={event.args.to} />. Thanks for the time,{' '}
          <EthereumAddress address={event.args.from} />!
        </p>
      ),
      dotClass: 'dot',
    }
  }

  // Named event from Rescue contract
  if (event.eventName == 'CatNamed' && event.eventContract == RESCUE_ADDRESS) {
    return {
      title: 'Named',
      description: (
        <p>
          This MoonCat was bestowed a name by <EthereumAddress address={event.tx.from} />.
        </p>
      ),
      dotClass: 'dot-large',
    }
  }

  // Adopted event from Rescue contract
  if (event.eventName == 'CatAdopted' && event.eventContract == RESCUE_ADDRESS) {
    if (event.args.to == ACCLIMATOR_ADDRESS) {
      // The address this MoonCat is going to is the Acclimator contract; this was an Acclimation, not adoption
      return {
        title: 'Acclimated',
        description: <p>This MoonCat visited the Acclimator and got accustomed to life on the modern blockchain!</p>,
        dotClass: 'dot',
      }
    }
    if (event.args.from == ACCLIMATOR_ADDRESS) {
      // The address this MoonCat is coming from is the Acclimator contract; this was Deacclimating, not adoption
      return {
        title: 'Deacclimated',
        description: (
          <p>
            This MoonCat took some time away from the hustle and bustle of modern blockchain life to spend some time
            returning to their roots.
          </p>
        ),
        dotClass: 'dot',
      }
    }
    if (ADDRESS_DETAILS[event.args.to] && ADDRESS_DETAILS[event.args.to].type == 'wrapper') {
      // The MoonCat is getting wrapped into some wrapper other than the Acclimator
      return {
        title: 'Mysterious Voyage',
        description: (
          <p>
            This MoonCat went with <EthereumAddress address={event.args.from} /> to spend some time at{' '}
            <EthereumAddress address={event.args.to} />.
          </p>
        ),
        dotClass: 'dot',
      }
    }
    if (ADDRESS_DETAILS[event.args.from] && ADDRESS_DETAILS[event.args.from].type == 'wrapper') {
      // The MoonCat is returning from being wrapped into some wrapper other than the Acclimator
      return {
        title: 'Returned from Voyage',
        description: (
          <p>
            This MoonCat returned to <EthereumAddress address={event.args.to} /> after spending some time at{' '}
            <EthereumAddress address={event.args.from} />.
          </p>
        ),
        dotClass: 'dot',
      }
    }
    if (event.args.from == ZERO_ADDRESS) {
      // The address this MoonCat is coming from is the Zero Address; this only happens with Genesis MoonCats when adopted for the first time
      return {
        title: 'Adopted by new owner',
        description: (
          <p>
            Got adopted by <EthereumAddress address={event.args.to} />.
          </p>
        ),
        dotClass: 'dot',
      }
    }

    return {
      title: 'Adopted by new owner',
      description: (
        <p>
          Got adopted by <EthereumAddress address={event.args.to} />. Thanks for the time,{' '}
          <EthereumAddress address={event.args.from} />!
        </p>
      ),
      dotClass: 'dot',
    }
  }

  // Rescued event from Rescue contract
  if (event.eventName == 'CatRescued' && event.eventContract == RESCUE_ADDRESS) {
    return {
      title: 'Rescued',
      description: (
        <p>
          As part of the Insanely Cute Operation, this MoonCat was rescued from the moon by{' '}
          <EthereumAddress address={event.args.to} />.
        </p>
      ),
      dotClass: 'dot-large',
    }
  }

  // Genesis release batch
  if (event.eventName == 'GenesisCatsAdded' && event.eventContract == RESCUE_ADDRESS) {
    return {
      title: 'Joined the Insanely Cute Operation',
      description: (
        <p>
          This <em>Genesis</em> MoonCat opted to join the Insanely Cute Operation, and help lead their brethren to
          ChainStation Alpha. Genesis MoonCats joined in batches of 16, and this was the moment the batch including this
          Genesis MoonCat joined in!
        </p>
      ),
      dotClass: 'dot-large',
    }
  }

  // MoonCatMoment participation
  if (event.eventName == 'MomentMint' && event.eventContract == MOMENTS_ADDRESS) {
    const moment = moments[event.args.momentId]
    let imageSrc = moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
    const dateDetail = formatTimestamp(moment.eventDate)

    return {
      title: 'Had a Moment',
      description: (
        <p>
          This MoonCat participated in{' '}
          <Link href={'/moments/' + moment.momentId}>{`MoonCatMoment #${moment.momentId}`}</Link>,{' '}
          <strong>{moment.meta.name}</strong> (event date {dateDetail})
        </p>
      ),
      image: imageSrc,
      dotClass: 'dot',
    }
  }

  // Deposit event from JumpPort contract
  if (event.eventName == 'Deposit' && event.eventContract == JUMPPORT_ADDRESS) {
    return {
      title: 'Visited JumpPort',
      description: (
        <p>
          This MoonCat visited the ChainStation JumpPort, to prepare for further explorations across the blockchain
          multiverse.
        </p>
      ),
      dotClass: 'dot',
    }
  }

  // Withdraw event from JumpPort contract
  if (event.eventName == 'Withdraw' && event.eventContract == JUMPPORT_ADDRESS) {
    return {
      title: 'Returned from JumpPort',
      description: <p>This MoonCat returned from their voyages through the ChainStation JumpPort.</p>,
      dotClass: 'dot',
    }
  }

  // An unknown event type?
  return {
    title: 'A Ripple in the Multiverse...',
    description: <p>This MoonCat had an un-quantifiable adventure of some sort on the blockchain...</p>,
    dotClass: 'dot',
  }
}

/**
 * Given a data blob from an Ethereum transaction, determine a name for the function that was called.
 *
 * The first four bytes of a transaction's data blob is the signature (hash) of the function that is triggered by the transaction.
 * This function could call something like https://www.4byte.directory/ to get a name for the function, but for this application's
 * purposes, having a static list of known signatures is sufficient, since the functions are from a known subset for the most part.
 */
const getFunctionName = function (txData: `0x${string}`): string {
  const fourBytes = txData.substring(0, 10)
  switch (fourBytes) {
    // Rescue
    case '0x4946e206':
      return 'rescueCat(bytes32 seed)'
    case '0xf884e54a':
      return 'giveCat(bytes5 catId, address to)'
    case '0x74fe6dea':
      return 'nameCat(bytes5 catId, bytes32 catName)'
    case '0x1be70510':
      return 'acceptAdoptionOffer(bytes5 catId)'
    case '0xa40c8ad0':
      return 'addGenesisCatGroup()'
    case '0xd4a03f60':
      return 'acceptAdoptionRequest(bytes5 catId)'

    // Wrappers/ERC721
    case '0x79b177ec':
      return 'wrap(bytes5 catId)'
    case '0xea598cb0':
      return 'wrap(uint256 rescueOrder)'
    case '0xde0e9a3e':
      return 'unwrap(uint256 tokenID)'
    case '0x440230d4':
      return 'batchWrap(uint256[] rescueOrders)'
    case '0x697b91e0':
      return 'batchReWrap(uint256[] rescueOrders, uint256[] oldTokenIds)'
    case '0xb88d4fde':
      return 'safeTransferFrom(address from, address to, uint256 tokenId, bytes data)'
    case '0x42842e0e':
      return 'safeTransferFrom(address from, address to, uint256 tokenId)'
    case '0x23b872dd':
      return 'transferFrom(address from, address to, uint256 tokenId)'

    // JumpPort
    case '0xa71604e8':
      return 'deposit(address tokenAddress, uint256[] tokenIds)'
    case '0x5058c460':
      return 'safeWithdraw(address tokenAddress, uint256 tokenId)'

    // Marketplaces
    case '0xab834bab':
      return 'atomicMatch_'
    case '0xa8174404':
      return 'matchOrders'
    case '0xb3a34c4c':
      return 'fulfillOrder'
    case '0xfb0f3ee1':
      return 'fulfillBasicOrder'
    case '0xe7acab24':
      return 'fulfillAdvancedOrder'
    case '0x87201b41':
      return 'fulfillAvailableAdvancedOrders'
    case '0xf2d12b12':
      return 'matchAdvancedOrders'
    case '0x8585ae03':
      return 'executeTakerBid'
    case '0xe72853e1':
      return 'executeTakerAsk'
    case '0x7034d120':
      return 'takeBid'
    case '0xda815cb5':
      return 'takeBidSingle'
    case '0x3925c3c3':
      return 'takeAsk'
    case '0x70bce2d6':
      return 'takeAskSingle'
    case '0x336d8206':
      return 'takeAskSinglePool'
    case '0xb4e4b296':
      return 'matchAskWithTakerBidUsingETHAndWETH'
    case '0x3b6d032e':
      return 'matchBidWithTakerAsk'
    case '0xb3be57f8':
      return 'bulkExecute'
    case '0x9a1fc3a7':
      return 'execute'
    case '0x9a2b8115':
      return 'batchBuyWithETH'
    case '0x09ba153d':
      return 'batchBuyWithERC20s'
    case '0xd65ef8ed':
      return 'swap721(uint256 in, uint256 out)'
    case '0x32389b71':
      return 'bulkTransfer(tuple[] items, bytes32 conduitKey)'
    case '0x38e29209':
      return 'matchAskWithTakerBid(tuple takerBid, tuple makerAsk)'

    // Multi-Sig
    case '0x3f801f91':
      return 'proxyAssert(address dest, uint8 howToCall, bytes calldata)'

    // Pools
    case '0x7e067a60':
      return 'deposit(uint256[] amounts, uint256[] minAmounts, address referral)'
    case '0xf8f0e295':
      return 'execute(address wallet, address[] to, uint256[] value, bytes[] data)'
    case '0x0e47f8d3':
      return 'batchTransferERC721(address[] to, address[] registry, uint256[] id)'

    case '0xac8ad13e':
      return 'mintClaimable(string uri, uint16[] rescueOrders)'

    default:
      return fourBytes
  }
}

const MoonCatTimeline = ({ events }: { events: BlockchainEvent[] }) => {
  // Insert labels into the timeline
  let timelineNodes: Array<TimelineEventNode | TimelineLabelNode> = []
  if (events.length > 0) {
    timelineNodes.push({
      type: 'event',
      data: events[0],
    })
    for (let i = 1; i < events.length; i++) {
      const previousEvent = events[i - 1]
      const curEvent = events[i]

      const previousDate = new Date(previousEvent.timestamp * 1000)
      const curDate = new Date(curEvent.timestamp * 1000)

      if (previousDate.getFullYear() != curDate.getFullYear()) {
        // These events span a year gap
        if (Number(previousDate.getFullYear()) - Number(curDate.getFullYear()) == 1) {
          timelineNodes.push({
            type: 'label',
            data: `Year ${curDate.getFullYear()} became ${previousDate.getFullYear()}...`,
          })
        } else {
          // More than a one-year gap
          const yearsDelta = (previousEvent.timestamp - curEvent.timestamp) / 60 / 60 / 24 / 365
          const yearsLabel = Math.floor(yearsDelta * 4) / 4
          timelineNodes.push({
            type: 'label',
            data: `${yearsLabel} years passed...`,
          })
        }
      }

      timelineNodes.push({
        type: 'event',
        data: curEvent,
      })
    }
  }

  return (
    <section className="timeline-container">
      <h2 style={{ paddingTop: '1rem' }}>Present Time...</h2>
      {timelineNodes.map((n) => {
        if (n.type == 'label') {
          return <h2 key={`label-${n.data}`}>{n.data}</h2>
        }
        const event = n.data
        const message = getMessageForEvent(event)
        const eventDate = new Date(event.timestamp * 1000)
        const shortDate = formatTimestamp(event.timestamp)
        return (
          <div key={event.blockNumber + '::' + event.logIndex} className="timeline-event">
            <div>
              <div className={message.dotClass} />
              <h3>
                {shortDate} &mdash; {message.title}
              </h3>
              {message.description}
              <p className="blockchain-details">
                <strong>Blockchain Details:</strong>
                <br />
                In block {event.blockNumber} ({eventDate.toISOString()}), as part of transaction{' '}
                <EthereumTransaction hash={event.tx.hash} />. Address <EthereumAddress address={event.tx.from} /> called
                function <code>{getFunctionName(event.tx.data)}</code> on contract{' '}
                <EthereumAddress address={event.tx.to} />, causing <code>{event.eventName}</code> event to be fired.
              </p>
            </div>
            {message.image && (
              <div className="thumbnail">
                <picture>
                  <img alt="" src={message.image} />
                </picture>
              </div>
            )}
          </div>
        )
      })}
      <h2 style={{ paddingBottom: '1rem' }}>The Beginning of It All...</h2>
    </section>
  )
}

const MoonCatTabTimeline = ({ moonCat, details }: TabProps) => {
  const [events, setEvents] = useState<BlockchainEvent[]>([])
  const [status, setStatus] = useFetchStatus()

  useEffect(() => {
    let ignore = false
    async function doFetchEvents() {
      setStatus('pending')
      const rs = await fetch(`${API2_SERVER_ROOT}/events?limit=50&mooncat=${moonCat.catId}`)
      if (ignore) return
      if (!rs.ok) {
        setStatus('error')
        console.error(rs)
        return
      }
      const data = (await rs.json()) as BlockchainEvent[]
      if (ignore) return

      let parsed = data.filter((e) => {
        // Skip transfer events on the Acclimator contract that are duplicated by more-specific events
        if (
          e.eventName == 'Transfer' &&
          e.eventContract == ACCLIMATOR_ADDRESS &&
          (e.args.from == ZERO_ADDRESS ||
            e.args.to == ZERO_ADDRESS ||
            e.args.from == JUMPPORT_ADDRESS ||
            e.args.to == JUMPPORT_ADDRESS)
        )
          return false

        // Skip transfer events that didn't actually go anywhere
        if (e.eventName == 'CatAdopted' && e.eventContract == RESCUE_ADDRESS && e.args.from == e.args.to) return false
        if (e.eventName == 'Transfer' && e.eventContract == ACCLIMATOR_ADDRESS && e.args.from == e.args.to) return false

        return true
      })

      // Find if any MoonCatMoments include them
      let doSort = false
      MOMENT_RELEASES.forEach((e) => {
        if (e.args.rescueOrders.includes(moonCat.rescueOrder)) {
          parsed.push(e)
          doSort = true
        }
      })
      if (doSort) {
        parsed.sort((a, b) => b.timestamp - a.timestamp)
      }

      setEvents(parsed)
      setStatus('done')
    }
    doFetchEvents()

    return () => {
      ignore = true
    }
  }, [moonCat.catId, moonCat.rescueOrder, setStatus])

  return (
    <>
      <div className="text-container">
        <section className="card-help">
          MoonCats have been around on the blockchain for quite a while now! Here&rsquo;s a record of all the things
          this MoonCat has done, starting from the most recent:
        </section>
      </div>
      {events.length > 0 && <MoonCatTimeline events={events} />}
      {status == 'pending' && <LoadingIndicator />}
    </>
  )
}
export default MoonCatTabTimeline
