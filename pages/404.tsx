import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import RandomMoonCatRow from 'components/RandomMoonCatRow'

const Acclimator: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>404 - Lost in Deep Space</title>
      </Head>
      <div className="text-container">
        <h1 className="hero">-- 404 -- </h1>
        <RandomMoonCatRow />
        <section className="card">
          <p>
            <strong>Lost in Deep Space!</strong> Navigation computer is showing you&rsquo;re now in the vacinity of the{' '}
            <em>Foroh Fore</em> nebula. You seem to have taken a wrong turn somewhere...
          </p>
        </section>
      </div>
    </div>
  )
}
export default Acclimator
