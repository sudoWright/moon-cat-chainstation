import React, { useCallback, useEffect, useState } from 'react'
import type { NextPage } from 'next'
import { AccessoryData } from 'lib/types'
import { ACCESSORIES_ADDRESS, API_SERVER_ROOT } from 'lib/util'
import Head from 'next/head'
import { useContractRead } from 'wagmi'
import { parseAbi } from 'viem'

const PER_PAGE = 50

const AccessoryThumb = ({ accessory }: { accessory: AccessoryData }) => {
  return (
    <div className="item-thumb">
      <a href={'/accessories/' + accessory.id}>
        <div
          className="thumb-img"
          style={{
            backgroundImage: `url(${API_SERVER_ROOT}/accessory-image/${accessory.id}?scale=3&padding=5)`,
          }}
        />
        <p>#{accessory.id}</p>
      </a>
    </div>
  )
}

interface FilterSettings {
  classification?: 'genesis' | 'rescue'
  facing?: 'right' | 'left'
}

const AccessoriesHome: NextPage = () => {
  const [accessories, setAccessories] = useState<Array<AccessoryData>>([])
  const [pageNumber, setCurrentPage] = useState<number>(0)
  const [filters, setFilters] = useState<FilterSettings>({})

  const {
    data: totalAccessories,
    isFetching,
    refetch,
  } = useContractRead({
    address: ACCESSORIES_ADDRESS,
    abi: parseAbi(['function totalAccessories() external view returns (uint256)']),
    functionName: 'totalAccessories',
  })

  const filterAccessories = useCallback(() => {
    if (isFetching || typeof totalAccessories == 'undefined') {
      setAccessories([])
      return
    }
    let accessories: AccessoryData[] = []
    for (let i = 0; i < totalAccessories; i++) {
      accessories.push({
        id: i,
      })
    }
    setAccessories(accessories.reverse())
  }, [totalAccessories, isFetching])

  useEffect(() => {
    filterAccessories()
  }, [filters, filterAccessories, totalAccessories])

  const pageTitle = 'Accessories'
  const pageHeader = (
    <>
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Browse the entire collection of stylish accessories for MoonCats"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">Accessories</h1>
        <section className="card">
          <p>
            As MoonCats are playful, fashionable, and always exploring, they&rsquo;ve found plenty of costumes, toys,
            and other accessories in their adventures through Deep Space. This evolved to a Boutique{' '}
            <a href="https://mooncat.community/blog/accessories-launch">was established</a> on ChainStation Alpha to
            allow creators of such fine wares to market them to all the MoonCats.
          </p>
        </section>
        <section className="card-help">
          <p>
            MoonCats are able to be customized by their owner by purchasing Accessories and choosing which are active on
            their MoonCat. Once a MoonCat has purchased an Accessory, they keep the right to wear that accessory forever
            (a MoonCat&rsquo;s <em>wardrobe</em> of available accessories goes with them, even if adopted by another
            owner). But they don&rsquo;t have to wear all their accessories all the time; their current owner can set
            which are visible at any time.
          </p>
        </section>
        <section className="card-notice">
          <p>
            <strong>Caution! This site is in early beta phase.</strong> While under construction, visit{' '}
            <a href="https://boutique.mooncat.community/">The Boutique</a> to purchase accessories.
          </p>
        </section>
      </div>
    </>
  )

  if (isFetching) {
    return (
      <div id="content-container">
        {pageHeader}
        <p>Loading accessory metadata...</p>
      </div>
    )
  }

  return (
    <div id="content-container">
      {pageHeader}
      <div id="filters">
        <button
          onClick={(e) => {
            refetch()
          }}
        >
          Refresh
        </button>
      </div>
      <div id="item-grid" style={{ margin: '2rem 0' }}>
        {accessories.slice(pageNumber * PER_PAGE, (pageNumber + 1) * PER_PAGE).map((acc) => (
          <AccessoryThumb key={acc.id} accessory={acc} />
        ))}
      </div>
      <div>
        <button
          onClick={(e) => {
            setCurrentPage((curr) => {
              return curr + 1
            })
          }}
        >
          Next
        </button>
      </div>
    </div>
  )
}
export default AccessoriesHome
