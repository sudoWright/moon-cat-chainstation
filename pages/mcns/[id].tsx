import React, { useContext, useEffect, useState } from 'react'
import type { NextPage, GetServerSideProps } from 'next'
import Head from 'next/head'
import MoonCatGrid from 'components/MoonCatSelectorGrid'
import EthereumAddress from 'components/EthereumAddress'
import LoadingIndicator from 'components/LoadingIndicator'
import { customEvent } from 'lib/analytics'
import {
  API_SERVER_ROOT,
  API2_SERVER_ROOT,
  AppVisitorContext,
  useFetchStatus,
  useIsMounted,
  switchToChain,
} from 'lib/util'
import { MoonCatData, OwnedMoonCat } from 'lib/types'
import useTxStatus from 'lib/useTxStatus'
import useTabHandler from 'lib/useTabHandler'
import { useAccount, useContractRead } from 'wagmi'
import { fetchEnsAddress, readContract } from 'wagmi/actions'
import { useWeb3Modal } from '@web3modal/wagmi/react'
import { getAddress, isAddress, parseAbi } from 'viem'

interface TabProps {
  ownedMoonCats: OwnedMoonCat[]
  needsAnnouncing?: number[]
  fetchNeedsAnnouncing: Function
}

interface Tab {
  label: string
  content: (props: TabProps) => JSX.Element
}

const AnnouncedMoonCats = ({ ownedMoonCats, needsAnnouncing }: TabProps) => {
  const isMounted = useIsMounted()
  const {
    state: { viewPreference },
  } = useContext(AppVisitorContext)
  const [status, setStatus] = useFetchStatus()
  const [moonCatData, setMoonCatData] = useState<MoonCatData[]>([])

  // Actions to take once; on mount
  useEffect(() => {
    if (typeof needsAnnouncing == 'undefined') return
    const moonCats = ownedMoonCats
      .filter((mc) => {
        if (mc.collection.address == '0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6') return false
        if (needsAnnouncing.includes(mc.rescueOrder)) return false
        return true
      })
      .map((mc) => mc.rescueOrder)
    let params = new URLSearchParams()
    params.set('limit', '200')
    params.set('mooncats', moonCats.join(','))
    fetch(`/api/mooncats?${params.toString()}`)
      .then((rs) => {
        return rs.json()
      })
      .then((data) => {
        setMoonCatData(data.moonCats)
        setStatus('done')
      })
      .catch((err) => {
        console.error(err)
        setStatus('error')
      })
  }, [ownedMoonCats, needsAnnouncing, setStatus])

  if (isMounted && typeof needsAnnouncing == 'undefined') return <LoadingIndicator />

  if (status == 'done') {
    if (moonCatData.length == 0) {
      return (
        <div className="text-container">
          <section className="card">
            <p>No MoonCats in this wallet have Announced their MCNS names yet.</p>
          </section>
        </div>
      )
    }
    return (
      <div style={{ display: 'flex', columnGap: '2em', flexWrap: 'wrap', justifyContent: 'space-evenly' }}>
        {moonCatData.map((moonCat) => {
          let thumbStyle: React.CSSProperties = { flex: '2 2 120px' }
          switch (viewPreference) {
            case 'accessorized':
              thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5)`
              break
            case 'mooncat':
              thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=)`
              break
            case 'face':
              thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=&headOnly)`
              break
          }
          let name = null
          if (typeof moonCat.name != 'undefined') {
            if (moonCat.name === true) {
              name = moonCat.catId
            } else {
              name = moonCat.name
            }
          } else {
            name = moonCat.catId
          }

          return (
            <section
              key={moonCat.rescueOrder}
              className="card"
              style={{
                flex: '1 1 auto',
                maxWidth: '500px',
                display: 'flex',
                alignItems: 'center',
                gap: '1em',
              }}
            >
              <div className="thumb-img" style={thumbStyle} />
              <div style={{ flex: '1 1 230px', textAlign: 'right' }}>
                <h3 style={{ margin: '0' }}>{name}</h3>
                <p style={{ fontSize: '0.8em' }}>
                  My owner can be found at:
                  <code>{moonCat.rescueOrder}.ismymooncat.eth</code>
                  <br />
                  <code>{moonCat.catId}.ismymooncat.eth</code>
                </p>
              </div>
            </section>
          )
        })}
      </div>
    )
  } else if (status == 'error') {
    return <div>ERROR</div>
  } else {
    return <LoadingIndicator message="Loading..." />
  }
}

const PendingMoonCats = ({ ownedMoonCats, needsAnnouncing, fetchNeedsAnnouncing }: TabProps) => {
  const isMounted = useIsMounted()
  const { isConnected } = useAccount()
  const { open } = useWeb3Modal()
  const [selectedMoonCats, setSelectedMoonCats] = useState<Set<bigint>>(new Set())
  const { viewMessage, setStatus, processTransaction } = useTxStatus()

  const handleClick = (mc: MoonCatData) => {
    const rescueOrder = BigInt(mc.rescueOrder)
    setSelectedMoonCats((current) => {
      let next = new Set(current)
      if (next.has(rescueOrder)) {
        next.delete(rescueOrder)
      } else {
        next.add(rescueOrder)
      }
      return next
    })
  }

  const handleAnnounce = async () => {
    if (selectedMoonCats.size == 0) {
      setStatus('error', 'No MoonCats selected')
      return
    }
    setStatus('building')
    if (!(await switchToChain(1))) {
      setStatus('error', 'Wrong network chain')
      return
    }

    const config = {
      address: '0x2eCcDA7C84837Da55ce34A36a5AD2B936479E76e' as `0x${string}`,
      abi: parseAbi([
        'function addr(bytes32 nodeID) external view returns (address)',
        'function addr(uint256 rescueOrder) external view returns (address)',
        'function mapMoonCat(uint256 rescueOrder) external',
        'function announceMoonCat(uint256 rescueOrder) external',
        'function mapMoonCats(uint256[] memory rescueOrders) external',
      ]),
    }
    let txConfig: Parameters<typeof processTransaction>[0]
    if (selectedMoonCats.size == 1) {
      // Determine if this MoonCat needs mapping, or just announcing
      const rescueOrder: bigint = selectedMoonCats.values().next().value
      let isMapped = true
      try {
        await readContract({
          ...config,
          functionName: 'addr',
          args: [rescueOrder],
        })
      } catch (err) {
        isMapped = false
      }
      if (isMapped === false) {
        txConfig = {
          ...config,
          functionName: 'mapMoonCats',
          args: [[rescueOrder]],
        }
      } else {
        txConfig = {
          ...config,
          functionName: 'announceMoonCat',
          args: [rescueOrder],
        }
      }
    } else {
      // Do a batch Announce
      const rescueOrders = Array.from(selectedMoonCats)
      txConfig = {
        ...config,
        functionName: 'mapMoonCats',
        args: [rescueOrders],
      }
    }
    let rs = await processTransaction(txConfig)
    if (rs) {
      customEvent('mcns_announce', {
        'mooncats': Array.from(selectedMoonCats).map((num) => Number(num)),
      })
      setSelectedMoonCats(new Set())
      fetchNeedsAnnouncing()
    }
  }

  const actionButton = isConnected ? (
    <button onClick={handleAnnounce}>Announce</button>
  ) : (
    <button onClick={() => open()}>Connect</button>
  )

  const label = selectedMoonCats.size == 1 ? 'MoonCat' : 'MoonCats'
  const selectionView =
    selectedMoonCats.size > 0 ? (
      <>
        <p>
          {selectedMoonCats.size} {label} selected for Announcing
        </p>
        <p>{actionButton}</p>
        {viewMessage}
      </>
    ) : (
      <p>Click to select some MoonCats to Announce</p>
    )

  return (
    <>
      <div className="text-container">
        <section className="card-help">
          <p>
            MCNS subdomains need to be <em>Announced</em>, in order for the wider ENS ecosystem to be made aware they
            exist (triggers on-chain Events that ENS-supporting tools watch for). A MCNS address only needs to be
            Announced again if the MoonCat changes wallets.
          </p>
        </section>
      </div>
      {isMounted && typeof needsAnnouncing == 'undefined' && <LoadingIndicator style={{ marginBottom: '1em' }} />}
      <MoonCatGrid
        moonCats={ownedMoonCats
          .filter(
            (mc) =>
              mc.collection.address.toLowerCase() != '0xc3f733ca98e0dad0386979eb96fb1722a1a05e69' ||
              needsAnnouncing?.includes(mc.rescueOrder)
          )
          .map((mc) => mc.rescueOrder)}
        apiPage="mooncats"
        minCellWidth={100}
        highlightedMoonCats={Array.from(selectedMoonCats).map((i) => Number(i))}
        disabledMoonCats={ownedMoonCats
          .filter((mc) => mc.collection.address.toLowerCase() == '0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6')
          .map((mc) => mc.rescueOrder)}
        onClick={handleClick}
      >
        {(mc: MoonCatData) => <p>#{mc.rescueOrder}</p>}
      </MoonCatGrid>
      <div className="text-container">
        <section className="card" style={{ textAlign: 'center' }}>
          {selectionView}
        </section>
      </div>
    </>
  )
}

const tabs = {
  'announced': {
    label: 'Announced',
    content: AnnouncedMoonCats,
  } as Tab,
  'pending': {
    label: 'Pending',
    content: PendingMoonCats,
  } as Tab,
}
type TabName = keyof typeof tabs
const defaultTab = 'announced'
function isTabName(name: string): name is TabName {
  return Object.keys(tabs).includes(name)
}

interface Props {
  ownerAddress: `0x${string}`
  moonCats: OwnedMoonCat[]
}

const MCNSOwnerPage: NextPage<Props> = ({ ownerAddress, moonCats }) => {
  const { activeTab, toggleTab } = useTabHandler(defaultTab)

  const humanAbi = ['function needsAnnouncing(address) external view returns (uint256[])'] as const
  const {
    data: needsAnnouncing,
    isError,
    isLoading,
    refetch: fetchNeedsAnnouncing,
  } = useContractRead({
    address: '0x2eCcDA7C84837Da55ce34A36a5AD2B936479E76e',
    abi: parseAbi(humanAbi),
    functionName: 'needsAnnouncing',
    args: [ownerAddress],
    cacheOnBlock: true,
    select: (rs) => rs.map((id) => Number(id)).sort((a, b) => a - b),
  })

  const pageTitle = `MoonCatNameService - ${ownerAddress}`
  const TabContent = isTabName(activeTab) ? tabs[activeTab].content : tabs[defaultTab].content

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
      </Head>
      <div className="text-container">
        <h1 className="hero">MoonCatNameService</h1>
        <p style={{ textAlign: 'center', marginTop: '-1em', fontSize: '1.5rem' }} className="text-scrim">
          <EthereumAddress address={ownerAddress} mode="bar" linkProfile={false} />
        </p>
        <section className="card">
          <p>
            MoonCats form close bonds with their Etherian owners, but while both are traveling the digital and physical
            metaverse realms, it can cause some insecurity for the MoonCats to know where their owners are at. To
            rectify this issue, the MoonCats tapped into a galactic beacon network, so their owners would be only a ping
            away!
          </p>
        </section>
      </div>
      <nav className="tabs">
        {Object.entries(tabs).map((tab) => (
          <div
            key={tab[0]}
            className={tab[0] == activeTab ? 'active' : ''}
            onClick={() => toggleTab(tab[0] as TabName)}
          >
            {tab[1].label}
          </div>
        ))}
      </nav>
      <section className="tab-contents">
        <TabContent
          ownedMoonCats={moonCats}
          needsAnnouncing={!isLoading && !isError ? needsAnnouncing : undefined}
          fetchNeedsAnnouncing={fetchNeedsAnnouncing}
        />
      </section>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const request = require('lib/request')

  if (typeof ctx.params == 'undefined') {
    console.error('Routing error')
    return { notFound: true }
  }
  const id = ctx.params.id
  if (Array.isArray(id) || typeof id == 'undefined' || id == 'undefined') {
    console.error('Routing error', id)
    return { notFound: true }
  }

  let targetAddress
  if (isAddress(id)) {
    targetAddress = getAddress(id) // Ensure address is in checksummed format
  } else {
    targetAddress = await fetchEnsAddress({ name: id })
  }

  let rs = await request(`${API2_SERVER_ROOT}/owner-profile/${targetAddress}`)
  if (rs.httpStatusCode != 200) {
    let err = JSON.parse(rs.body.toString('utf8'))
    console.error('Ownership info fetch error', err)
    return { notFound: true }
  }
  let owned: OwnedMoonCat[] = JSON.parse(rs.body.toString('utf8')).ownedMoonCats

  return {
    props: {
      ownerAddress: targetAddress,
      moonCats: owned,
    },
  }
}

export default MCNSOwnerPage
