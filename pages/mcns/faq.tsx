import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import Icon from 'components/Icon'
import { ZWS } from 'lib/util'

const pageTitle = 'MoonCatNameService - FAQ'
const MoonCatNameService: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Ethereum Name Service (ENS) domains for each MoonCat"
        />
      </Head>
      <nav className="breadcrumb">
        <Link href="/mcns">
          <a>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            MCNS Home
          </a>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">MoonCat{ZWS}NameService FAQ</h1>
        <section className="card-help">
          <h3>What is the MoonCat{ZWS}NameService?</h3>
          <p>
            Known as MCNS for short, the MoonCat{ZWS}NameService allows you to claim an{' '}
            <a target="_blank" rel="noreferrer" href="https://ens.domains">
              ENS domain
            </a>{' '}
            beginning with your MoonCat Mint ID <em>AND</em> Hex ID and ending with <code>ismymooncat.eth</code>!
          </p>
          <p>
            For example, the wallet holding MoonCat #84 can be accessed at <code>84.ismymooncat.eth</code> &
            <code>0xff00000ca7.ismymooncat.eth</code> in supported applications (including{' '}
            <a target="_blank" rel="noreferrer" href="//metamask.io">
              MetaMask
            </a>{' '}
            &{' '}
            <a target="_blank" rel="noreferrer" href="//etherscan.io">
              Etherscan
            </a>
            )
          </p>
          <h3>What is an ENS domain?</h3>
          <p>
            Think of it like a website URL for the Ethereum blockchain! It maps a human-readable name to
            machine-readable identifier such as your ETH wallet. You can read more about the{' '}
            <a target="_blank" rel="noreferrer" href="https://ens.domains">
              Ethereum Name Service on their website
            </a>
            .
          </p>
          <h3>So... is this a real ENS domain?</h3>
          <p>
            You bet! Wallets and websites that recognize ENS names should detect this new one pointing at your wallet
            once you announce it. The only difference between this and a standard ENS name is it is locked to only be
            able to point to the address that holds a specific MoonCat.
          </p>
          <h3>Am I eligible for the MoonCat{ZWS}NameService?</h3>
          <p>
            If you own an Acclimated MoonCat, you can claim their unique ETH domain name! If your MoonCat is not yet in
            the official Acclimated wrapper, you can wrap it quickly here for free (just pay gas):{' '}
            <Link href="/acclimator">MoonCat Acclimator</Link>
          </p>
          <p>
            Even if you&rsquo;re a late acclimator, your MoonCat{ZWS}NameService is free, so you can wrap today, and
            immediately get your new domain!
          </p>
          <h3>Great! I have a MoonCat in the official Acclimated Wrapper, now what?</h3>
          <p>
            Head to the <Link href="/mcns">{`MoonCat${ZWS}NameService`}</Link>, connect your wallet, select which
            MoonCat addresses you want to claim, and click the &ldquo;Announce&rdquo; button. It&rsquo;s free (just pay
            gas)
          </p>
          <p>
            For the 96 Genesis MoonCats, you&rsquo;re the pick of the litter, and your <code>ismymooncat.eth</code>{' '}
            domains have been activated already by the MoonCat{ZWS}Rescue team (provided it is acclimated). If it was
            not Acclimated, when you do, you will just have to announce it.
          </p>
          <p>
            The first time a MoonCat goes through the process, you&rsquo;ll be adding that MoonCat&rsquo;s ID numbers to
            the registry, similar to registering a <code>.com</code> domain! Once claimed, it will point to the address
            affiliated with that MoonCat.
          </p>
          <h3>How do I use the MoonCat{ZWS}NameService?</h3>
          <p>
            Head to the <Link href="/mcns">MoonCat NameService</Link> and connect your wallet with the{' '}
            <em>Connect Web3</em> button; we currently support both MetaMask as well as WalletConnect.
          </p>
          <p>
            Once your wallet is connected, select the MoonCats for the domains you&rsquo;d like to claim &mdash;
            it&rsquo;ll be called &ldquo;announcing&rdquo; on the MoonCat{ZWS}NameService page. You&rsquo;ll need to
            confirm the transaction with your wallet to finalize the claim (and confirm the gas fee); once that
            transaction is completed, you&rsquo;ll get a screen showing which domains you&rsquo;ve registered!
          </p>
          <h3>
            I bought a MoonCat and it seems like the former owner claimed the <code>ismymooncat.eth</code> domain! Can I
            get it back?
          </h3>
          <p>
            Absolutely! Head to the <Link href="/mcns">{`MoonCat${ZWS}NameService`}</Link> and connect your wallet. As
            the new owner, you&rsquo;ll be able to trigger an updated &ldquo;announcement&rdquo; (a lighter-gas action
            than the original &ldquo;claiming&rdquo;) to just update the existing record to the new owner.
          </p>
          <h3>What is &ldquo;Announcing&rdquo;?</h3>
          <p>
            Announcing is a fancy way of saying you&rsquo;re claiming an <code>.eth</code> domain! Right now, you can
            claim your <code>ismymooncat.eth</code> and use it for a wallet &mdash; in the neat future, we&rsquo;ll also
            be allowing you to add text data to your <code>ismymooncat.eth</code> domain, so you can further customize
            your MoonCat&rsquo;s domain! After the first announcement, subsequent announcements (if the MoonCat changes
            wallets) require much less gas to submit.
          </p>
        </section>
      </div>
    </div>
  )
}
export default MoonCatNameService
