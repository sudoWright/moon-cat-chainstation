import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import ENSDomainsList from 'components/ENSDomainsList'
import SuperfluidCheckout from 'components/SuperfluidCheckout'
import useTabHandler from 'lib/useTabHandler'
import { IPFS_GATEWAY, ZWS } from 'lib/util'

interface Tab {
  label: string
  content: () => JSX.Element
}

const FinancialTab = () => {
  return (
    <section className="card-help">
      <p>
        The trend in the NFT space has been away from giving NFT collection owners a royalty fee off of each sale of
        NFTs in their collection, and the MoonCat{ZWS}Rescue team has seen that even when marketplaces do give royalties
        to collection owners, that revenue stream can be very unpredictable. The MoonCat{ZWS}Rescue project is unlike
        many other NFT projects in that the initial launch did <strong>not</strong> earn the creators a large nest egg
        to work from. The initial profit-making aspect for the team (adoption costs of Genesis MoonCats) ended up not
        happening due to a bug in the contract, but the original creators let the project continue. No MoonCats in the
        collection were &ldquo;set aside&rdquo; for the creators to profit from later. The current MoonCat{ZWS}Rescue
        team works on a volunteer basis, but if additional funding sources were secured, more time and effort could be
        dedicated to the project.
      </p>
      <h3>ENS extensions</h3>
      <p>
        The MoonCat{ZWS}Rescue ecosystem uses a few <a href="https://ens.domains/">Ethereum Name Service (ENS)</a> names
        as anchors and additional utility within the ecosystem. ENS names do need to be renewed for a fixed cost in
        order to keep their ownership. But anyone (not just the owner of the name) can pay to renew/extend the
        name&rsquo;s time.
      </p>
      <ENSDomainsList />

      <h3>Discord Boosts</h3>
      <p>
        The <a href="http://discord.gg/mooncats">MoonCat{ZWS}Rescue Discord Server</a> is a great resource for meeting
        others interested in the MoonCat{ZWS}Rescue project, and helping educate newcomers to the NFT ecosystem. In
        order to keep some of the upgraded{' '}
        <a href="https://support.discord.com/hc/en-us/articles/360028038352-Server-Boosting-FAQ-#h_419c3bd5-addd-4989-b7cf-c7957ef92583">
          perks
        </a>{' '}
        of that server (most notably, the ability to have a customized invite link), a certain number of
        &ldquo;Boosts&rdquo; need to be maintained. If the community covered those Boosts, that&rsquo;s more funds that
        the MoonCat{ZWS}Rescue team could allocate to other uses.
      </p>

      <h3>Get Some Swag</h3>
      <p>
        If you purchase an Accessory for your MoonCat from{' '}
        <a href="https://boutique.mooncat.community/">the Boutique</a>, a portion of the sale goes to the MoonCat{ZWS}
        Rescue team. Additionally, if you purchase an accessory{' '}
        <a href="https://boutique.mooncat.community/c/0xdf2E60Af57C411F848B1eA12B10a404d194bce27">
          owned by the MoonCat{ZWS}Rescue team
        </a>
        , <em>all</em> the proceeds go to the MoonCat{ZWS}Rescue team to continue building in the space. So, grabbing an{' '}
        <a href="https://boutique.mooncat.community/a/31/acclimator-pad-1">Acclimator pad</a> (
        <a href="https://boutique.mooncat.community/a/32/acclimator-pad-2">or two</a>), or a{' '}
        <a href="https://boutique.mooncat.community/a/35/moon">Moon</a> for your MoonCat to pose in front of is a great
        way to make a contribution and get some stylish swag as well!
      </p>

      <h3>Superfluid Sponsorship</h3>
      <SuperfluidCheckout />

      <h3>Infrastructure tokens</h3>
      <p>
        The MoonCat{ZWS}Rescue team creates things with an eye toward permanence and decentralization where possible.
        There&rsquo;s a few centralized services that the MoonCat ecosystem projects use to make things more easily
        accessible to &ldquo;Web 2.0&rdquo;-style services, as the Web 3.0 ecosystem continues to grow. In order to more
        fully decentralize those things, the available infrastructures to do so require some compensation in their own
        infrastructure tokens. If you&rsquo;re interested in the MoonCat{ZWS}Rescue team pursuing using these more
        decentralized infrastructure options, giving a donation of those tokens to the main MoonCat{ZWS}Rescue LLC
        address (<code>0xdf2E60Af57C411F848B1eA12B10a404d194bce27</code>) would be a good signal for that, and
        we&rsquo;d use those funds to further explore those hosting options.
      </p>
      <ul>
        <li>
          <a href="https://thegraph.com/en/">The Graph</a>: Infrastructure to make querying comple blockchain data more
          simple by creating a GraphQL front-end for it. <a href="https://www.coingecko.com/en/coins/the-graph">GRT</a>{' '}
          is their infrastructure token that will be used for querying and indexing of blockchain data.
        </li>
        <li>
          <a href="https://nftx.io/">NFTX</a>: As a platform, NFTX provides the means to have liquidity pools for NFTs,
          and there are two that are related to MoonCats currently (
          <a href="https://nftx.io/vault/0x98968f0747e0a261532cacc0be296375f5c08398/buy/">MOONCAT</a> and{' '}
          <a href="https://nftx.io/vault/0xa8b42c82a628dc43c2c2285205313e5106ea2853/buy/">MCAT17</a>). That
          infrastructure allows users to stake the pool tokens, as a way to lock those assets in place (ensuring the
          pool will never sink below that size). If you donate{' '}
          <a href="https://app.sushi.com/swap?inputCurrency=ETH&outputCurrency=0x98968f0747E0A261532cAcC0BE296375F5c08398&chainId=1">
            MOONCAT
          </a>{' '}
          or{' '}
          <a href="https://app.sushi.com/swap?inputCurrency=ETH&outputCurrency=0xA8b42C82a628DC43c2c2285205313e5106EA2853&chainId=1">
            MCAT17
          </a>{' '}
          to the MoonCat{ZWS}Rescue team, we&rsquo;ll hold it long-term (indirectly reducing the supply of available
          MoonCats, which could spur greater market prices), and profits gleaned from such tokens (staking rewards)
          could become prizes and rewards for community events in the future.
        </li>
      </ul>
    </section>
  )
}

const SkillsTab = () => {
  return (
    <section className="card-help">
      <h3>Technical / Software development</h3>
      <p>
        The code repositories for the MoonCat{ZWS}Rescue-created projects are hosted{' '}
        <a href="https://gitlab.com/mooncatrescue">on GitLab</a>. In those repositories, there are &ldquo;issues&rdquo;
        posted with various bug fixes, feature enhancements, and discussion topics that could use feedback from other
        developers. If you are familiar with git (for version control), solidity or hardhat (smart contract
        develompent), or React, Next, Vue, or Nuxt (front-end UI libraries), you&rsquo;ll likely find several things you
        can help contribute to!
      </p>
      <p>
        Start by perusing the <a href="https://gitlab.com/mooncatrescue/dev-environment">Developer Environment</a>{' '}
        repository, and the Architecture Decision Records (ADRs) posted there. Then feel free to dive in making a
        contribution and submitting a merge request. If you&rsquo;d like to discuss your changes with others, feel free
        to jump into <a href="http://discord.gg/mooncats">our Discord server</a>.
      </p>
      <h3>IPFS Pinning</h3>
      <p>
        The <a href="https://ipfs.tech/">IPFS</a> protocol helps distribute content in a decentralized manner, and as
        long as that content is static, anyone can help by hosting and re-sharing that content. Similar to
        &ldquo;seeding&rdquo; a torrent file, in the IPFS ecosystem, it&rsquo;s termed &ldquo;pinning&rdquo; the
        content. If you run your own IPFS node, or use a hosted IPFS service (like{' '}
        <a href="https://www.pinata.cloud/">Pinata</a> or <a href="https://infura.io/product/ipfs">Infura IPFS</a>),
        adding these content identifiers (CIDs) to your list of pinned content would help this content be more
        readily-available to everyone browsing for it.
      </p>
      <ul>
        <li>
          <code>mooncatparser.js</code>: This is the original Javascript implementation of the MoonCat{ZWS}Rescue
          visualizer; the script that is referenced by hash value in the original smart contract. As long as this file
          is still accessible, the original visualization of the MoonCat assets can be verified.
          <br />
          <a href={IPFS_GATEWAY + '/ipfs/bafybeifl4bvfa5qv3s4peni6fwdyggdfbcykm2xe77z5bfpmureec2x7oq'}>
            <code>bafybeifl4bvfa5qv3s4peni6fwdyggdfbcykm2xe77z5bfpmureec2x7oq</code>
          </a>
        </li>
        <li>
          <strong>MoonCat Walk Sprites</strong>: A folder containing a PNG file for each rescued MoonCat that is a
          sprite sheet for a walk animation formatted to work with <a href="https://webb.game/">Worldwide Web3</a>{' '}
          (generated by the collaborators on{' '}
          <a href="https://github.com/sudoWright/mooncatrescue-walkcycles">this repository</a>).
          <br />
          <a href={IPFS_GATEWAY + '/ipfs/bafybeib5iedrzr7unbp4zq6rkrab3caik7nw7rfzlcfvu4xqs6bfk7dgje'}>
            <code>bafybeib5iedrzr7unbp4zq6rkrab3caik7nw7rfzlcfvu4xqs6bfk7dgje</code>
          </a>
        </li>
        <li>
          <strong>MoonCat Hex Explainers</strong>: A folder containing a PNG file for each rescued MoonCat that is a
          card that explains how that MoonCat&rsquo;s five-byte identifier stores the MoonCat&rsquo;s pose, pattern,
          expression, facing direction, and coat saturation traits.
          <br />
          <a href={IPFS_GATEWAY + '/ipfs/bafybeicp3ke3rrhakwlre4gexzcjx7uxotvtscda7kz3wdbkxa5usrbmwu'}>
            <code>bafybeicp3ke3rrhakwlre4gexzcjx7uxotvtscda7kz3wdbkxa5usrbmwu</code>
          </a>
        </li>
        <li>
          <strong>MoonCat{ZWS}Moments</strong>: The MoonCat{ZWS}Moments collection of fun visual memories different
          collections of MoonCats participated in has its public metadata hosted on IPFS. Pinning this one CID will pin
          a folder that has all the public metadata needed for that project.
          <br />
          <a href={IPFS_GATEWAY + '/ipfs/bafybeif6zs3hyywy5tuftfo3srfmgpdfpkphdodikchsaunex6bcsb4w3e'}>
            <code>bafybeif6zs3hyywy5tuftfo3srfmgpdfpkphdodikchsaunex6bcsb4w3e</code>
          </a>
        </li>
        <li>
          <strong>MoonCat{ZWS}Pop</strong>: The fizzy virtual drink derivative art pieces that let MoonCats be
          &ldquo;spokescats&rdquo; for their own drink line host their imagery on IPFS:
          <ul>
            <li>
              JSON metadata:
              <a href={IPFS_GATEWAY + '/ipfs/bafybeigygqykfizjo7mulfu5nwmfnehfff6dl52byt3ycude5j4jpufdt4'}>
                <code>bafybeigygqykfizjo7mulfu5nwmfnehfff6dl52byt3ycude5j4jpufdt4</code>
              </a>
            </li>
            <li>
              Still thumbnails:
              <a href={IPFS_GATEWAY + '/ipfs/bafybeiazpg4ahet2rbarshsehriyxpepaqbpr2zdrmtvy55dzqxsupcs4u'}>
                <code>bafybeiazpg4ahet2rbarshsehriyxpepaqbpr2zdrmtvy55dzqxsupcs4u</code>
              </a>
            </li>
            <li>
              Animated videos:
              <a href={IPFS_GATEWAY + '/ipfs/bafybeiec32zgd2zn7betyzl4tnbx72gvgxwjlkjy2v5qxhqdqcu7wvhdnq'}>
                <code>bafybeiec32zgd2zn7betyzl4tnbx72gvgxwjlkjy2v5qxhqdqcu7wvhdnq</code>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </section>
  )
}

const SocialTab = () => {
  return (
    <section className="card-help">
      <p>
        With over 25,000 MoonCats in the MoonCat&#8203;Rescue collection, it&rsquo;s one of the bigger NFT communities
        that exist. The NFT ecosystem overall right now is somewhat similar to an athetic sport, in that there&rsquo;s
        many different &ldquo;teams&rdquo; that many different groups of &ldquo;fans&rdquo; are rooting for, but we can
        generally identify other teams by their &ldquo;jerseys&rdquo;, and even if you and I have different
        &ldquo;favorite teams&rdquo;, seeing our sport gain interest benefits us both, and we can still be friends while
        supporting different favorite teams.
      </p>
      <p>
        Being active in the NFT space and &ldquo;liking&rdquo;{' '}
        <a href="https://twitter.com/mooncatrescue">MoonCat&#8203;Rescue</a> and{' '}
        <a href="https://twitter.com/hashtag/mooncats">other MoonCat-supporters</a> posts can have a tremendous
        snowballing effect, due to how many MoonCat holders and fans are out there. Stepping up to coordinate social
        get-togethers, meme contests, or other events to show off our favorite feline friends can be a great help to the
        community.
      </p>
      <p>
        Using virtual spaces like <a href="https://isotile.com/">isotile</a> or{' '}
        <a href="https://webb.game/">Worldwide Web3</a> (where MoonCats have a physical presence/representation) are
        free to use, so have a birthday party, or holiday celebration, or casual get-together there, and show what a
        &ldquo;metaverse pet&rdquo; can really be! If you have an idea for a social get-together and are interested in
        hosting the event, and need help finding a (virtual) space to host it, the MoonCat&#8203;Rescue team can help
        with that! Have an idea but not sure if you can actually pull it off? Hop on into{' '}
        <a href="https://discord.gg/mooncats">the Discord server</a> and post your idea, and you might find several
        friends there willing to help out!
      </p>
    </section>
  )
}

const tabs = {
  'financial': {
    label: 'Financial',
    content: FinancialTab,
  } as Tab,
  'skills': {
    label: 'Skills',
    content: SkillsTab,
  } as Tab,
  'social': {
    label: 'Social',
    content: SocialTab,
  } as Tab,
}
type TabName = keyof typeof tabs
const defaultTab = 'financial'
function isTabName(name: string): name is TabName {
  return Object.keys(tabs).includes(name)
}

const ContributePage: NextPage = () => {
  const { activeTab, toggleTab } = useTabHandler(defaultTab)

  const TabContent = isTabName(activeTab) ? tabs[activeTab].content : tabs[defaultTab].content

  return (
    <div id="content-container">
      <Head>
        <title>Contribute</title>
        <meta property="og:title" content="Contribute" />
        <meta
          name="description"
          property="og:description"
          content="Details on various community initiatives and ways to support the project as it grows"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">Contribute</h1>
        <section className="card-help">
          <p>
            The MoonCat{ZWS}Rescue project has grown a bunch since it launched in 2017 (more information about the
            project overall <Link href="/team/about">over here</Link>), and the current team of maintainers continues to
            raise awareness about the project, educate newcomers about NFTs in general, and coordinate added utility to
            the MoonCat ecosystem projects.
          </p>

          <p>
            If you&rsquo;d like to see this project continue to grow and evolve, there&rsquo;s many ways you can chip in
            funds, skills, and/or social awareness that would be greatly appreciated!
          </p>
        </section>
        <nav className="tabs">
          {Object.entries(tabs).map((tab) => (
            <div
              key={tab[0]}
              className={tab[0] == activeTab ? 'active' : ''}
              onClick={() => toggleTab(tab[0] as TabName)}
            >
              {tab[1].label}
            </div>
          ))}
        </nav>
        <section className="tab-contents">
          <TabContent />
        </section>
      </div>
    </div>
  )
}
export default ContributePage
