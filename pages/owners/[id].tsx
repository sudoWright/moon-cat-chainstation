import React, { useContext } from 'react'
import type { NextPage, GetServerSideProps } from 'next'
import Head from 'next/head'
import MoonCatGrid from 'components/MoonCatGrid'
import EthereumAddress from 'components/EthereumAddress'
import SignIn from 'components/SignIn'
import {
  ADDRESS_DETAILS,
  API2_SERVER_ROOT,
  useIsMounted,
  AppVisitorContext,
  getCurrentEvent,
  EcosystemEvent,
} from 'lib/util'
import { useAccount, Address } from 'wagmi'
import { fetchEnsAddress } from 'wagmi/actions'
import { getAddress, isAddress } from 'viem'
import { OwnedMoonCat } from 'lib/types'
import { MomentsOwned } from 'components/MomentsOwned'

interface Props {
  ownerAddress: Address
  moonCats: number[]
  currentEvent: EcosystemEvent | null
}

const OwnerDetailsPage: NextPage<Props> = ({ ownerAddress, moonCats, currentEvent }) => {
  const isMounted = useIsMounted()
  const { address, isConnected } = useAccount()
  const {
    state: {
      verifiedAddresses: { value: verifiedAddresses },
    },
  } = useContext(AppVisitorContext)

  const isOwnWallet =
    isMounted && // The page is ready to render...
    address == ownerAddress && // And the address of the owner currently being viewed is currently connected...
    verifiedAddresses.includes(address) // And that address is verified.

  let litterSummary: React.ReactNode
  if (ADDRESS_DETAILS[ownerAddress]?.type == 'pool') {
    // This is an NFT pool address
    litterSummary = (
      <>
        This is an NFT pool address; all of these MoonCats are up for adoption <em>right now</em>! If you&rsquo;re
        interested in adopting any of these {moonCats.length} MoonCats, head over to{' '}
        <a href={ADDRESS_DETAILS[ownerAddress].link} target="_blank" rel="noreferrer">
          the pool&rsquo;s website
        </a>{' '}
        to do so.
      </>
    )
  } else if (moonCats.length == 1) {
    if (isOwnWallet) {
      litterSummary = <>You are the proud owner of a MoonCat!</>
    } else {
      litterSummary = <>This address is the proud owner of a MoonCat!</>
    }
  } else if (moonCats.length > 0) {
    if (isOwnWallet) {
      litterSummary = <>You are the proud owner of {moonCats.length} MoonCats!</>
    } else {
      litterSummary = <>This address is the proud owner of {moonCats.length} MoonCats!</>
    }
  } else {
    if (isOwnWallet) {
      litterSummary = <>You don&rsquo;t own any MoonCats, currently.</>
    } else {
      litterSummary = <>This address doesn&rsquo;t own any MoonCats, currently.</>
    }
  }

  let ownerPrompt: React.ReactNode
  if (isMounted && isConnected && address == ownerAddress) {
    ownerPrompt = <SignIn address={address} />
  }

  const pageTitle = `Owner Profile - ${ownerAddress}`

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
      </Head>
      <div className="text-container">
        <h1 className="hero">Owner Profile</h1>
        <p style={{ textAlign: 'center', marginTop: '-1em', fontSize: '1.5rem' }} className="text-scrim">
          <EthereumAddress address={ownerAddress} mode="bar" linkProfile={false} />
        </p>
        {ownerPrompt}
        <section className="card">{litterSummary}</section>
      </div>
      {moonCats.length > 0 && (
        <MoonCatGrid moonCats={moonCats} apiPage="mooncats" isEventActive={currentEvent != null} />
      )}
      <MomentsOwned address={ownerAddress} />
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const request = require('lib/request')

  // Get the ID to look up (Ethereum address) from the request parameters
  if (typeof ctx.params == 'undefined') {
    console.error('Routing error')
    return { notFound: true }
  }
  const id = ctx.params.id
  if (Array.isArray(id) || typeof id == 'undefined' || id == 'undefined') {
    console.error('Routing error', id)
    return { notFound: true }
  }

  let targetAddress
  if (isAddress(id)) {
    targetAddress = getAddress(id) // Ensure address is in checksummed format
  } else {
    targetAddress = await fetchEnsAddress({ name: id })
    if (targetAddress == null) {
      console.error('Failed to look up ENS address', id)
      return { notFound: true }
    }
  }

  // Get a listing of all MoonCats owned by that address
  let rs = await request(`${API2_SERVER_ROOT}/owner-profile/${targetAddress}`)
  if (rs.httpStatusCode != 200) {
    let err = JSON.parse(rs.body.toString('utf8'))
    console.error('Ownership info fetch error', err)
    return { notFound: true }
  }
  let owned: OwnedMoonCat[] = JSON.parse(rs.body.toString('utf8')).ownedMoonCats

  // Fetch events listing from the API server
  const currentEvent = await getCurrentEvent()

  return {
    props: {
      ownerAddress: targetAddress,
      moonCats: owned.map((moonCat) => {
        return moonCat.rescueOrder
      }),
      currentEvent,
    },
  }
}

export default OwnerDetailsPage
