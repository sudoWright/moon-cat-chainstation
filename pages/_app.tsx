import 'styles/globals.scss'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { Reducer, useEffect, useReducer } from 'react'
import BackgroundStars from 'components/BackgroundStars'
import Footer from 'components/Footer'
import Navigation from 'components/Navigation'

import {
  getViewPreference,
  doUserCheck,
  PREFERENCE_MOONCATVIEW_KEY,
  PREFERENCE_MOONCATVIEWCHANGE_KEY,
  AppGlobalActionType,
  AppGlobalState,
  AppGlobalAction,
  AppVisitorContext,
} from 'lib/util'
import { GoogleAnalytics, pageview } from 'lib/analytics'

import { createWeb3Modal } from '@web3modal/wagmi/react'
import { walletConnectProvider, EIP6963Connector } from '@web3modal/wagmi'
import { configureChains, createConfig, Chain, WagmiConfig } from 'wagmi'
import { arbitrum, mainnet, polygon } from 'wagmi/chains'
import { infuraProvider } from 'wagmi/providers/infura'
import { publicProvider } from 'wagmi/providers/public'
import { CoinbaseWalletConnector } from 'wagmi/connectors/coinbaseWallet'
import { InjectedConnector } from 'wagmi/connectors/injected'
import { WalletConnectConnector } from 'wagmi/connectors/walletConnect'

const hardhat = {
  id: 1337,
  name: 'Hardhat (Mainnet)',
  network: 'hardhat',
  nativeCurrency: mainnet.nativeCurrency,
  rpcUrls: {
    public: { http: ['http://127.0.0.1:29990'] },
    default: { http: ['http://127.0.0.1:29990'] },
  },
  contracts: mainnet.contracts,
} as Chain

const walletConnectProject = '705e98fb7f922815cbb7c1e82e0fbb5a'
// Wagmi client
const { publicClient, chains } = configureChains(
  [mainnet, arbitrum, polygon, hardhat],
  [
    walletConnectProvider({ projectId: walletConnectProject }),
    infuraProvider({ apiKey: '9aa3d95b3bc440fa88ea12eaa4456161' }),
    publicProvider(),
  ]
)
const metadata = {
  name: 'MoonCatRescue ChainStation',
  description: 'Playground of the colorful felines rescued from the moon',
  url: 'https://chainstation.mooncatrescue.com',
  icons: ['https://chainstation.mooncatrescue.com/img/avatar.jpg'],
}
const wagmiConfig = createConfig({
  autoConnect: true,
  connectors: [
    new WalletConnectConnector({ chains, options: { projectId: walletConnectProject, showQrModal: false, metadata } }),
    new EIP6963Connector({ chains }),
    new InjectedConnector({ chains, options: { shimDisconnect: true } }),
    new CoinbaseWalletConnector({ chains, options: { appName: metadata.name } }),
  ],
  publicClient,
})

// Web3Modal
createWeb3Modal({
  wagmiConfig,
  projectId: walletConnectProject,
  chains,
  enableAnalytics: false,
})

/**
 * Application global state management reducer function
 */
const appReducer: Reducer<AppGlobalState, AppGlobalAction> = function (state, action) {
  const { type, payload } = action
  switch (type) {
    case AppGlobalActionType.SET_VIEW_PREFERENCE:
      // User updated their preference for what view of MoonCats to show
      if (payload != null) {
        // Save to local storage, to make this value persistent
        localStorage.setItem(PREFERENCE_MOONCATVIEW_KEY, payload)
        localStorage.setItem(PREFERENCE_MOONCATVIEWCHANGE_KEY, new Date().getTime().toString())
      }
      return {
        ...state,
        viewPreference: payload,
      }
    case AppGlobalActionType.UPDATE_VERIFIED_ADDRESSES:
      // List of addresses the visitor has proven ownership of has changed
      return {
        ...state,
        verifiedAddresses: payload,
      }
    default:
      return state
  }
}

const startingViewPreference = getViewPreference()

function ChainStationApp({ Component, pageProps }: AppProps) {
  const [state, dispatch] = useReducer(appReducer, {
    verifiedAddresses: {
      status: 'start',
      value: [],
    },
    viewPreference: startingViewPreference,
  })
  const router = useRouter()

  useEffect(() => {
    // When the visitor navigates to a new screen, log it as a pageview
    const handleRouteChange = (url: string) => {
      pageview(url)
    }

    router.events.on('routeChangeComplete', handleRouteChange)

    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  useEffect(() => {
    // When the overall application interface is finished being mounted,
    // kick off a request to see if a user is logged-in
    if (dispatch) doUserCheck(dispatch)
  }, [dispatch])

  return (
    <>
      <Head>
        <title>MoonCatRescue</title>
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="MoonCatRescue ChainStation Alpha" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <GoogleAnalytics />
      <WagmiConfig config={wagmiConfig}>
        <AppVisitorContext.Provider value={{ state, dispatch }}>
          <Navigation />
          <div id="content">
            <Component {...pageProps} />
            <Footer />
          </div>
        </AppVisitorContext.Provider>
        <BackgroundStars />
      </WagmiConfig>
    </>
  )
}

export default ChainStationApp
