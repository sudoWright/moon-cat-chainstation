import EthereumAddress from 'components/EthereumAddress'
import Icon from 'components/Icon'
import LoadingIndicator from 'components/LoadingIndicator'
import MoonCatGrid from 'components/MoonCatGrid'
import { Moment } from 'lib/types'
import useMomentOwners from 'lib/useMomentOwners'
import { API_SERVER_ROOT, EcosystemEvent, IPFS_GATEWAY, formatTimestamp, getCurrentEvent } from 'lib/util'
import type { NextPage, GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'

interface Props {
  moment: Moment
  currentEvent: EcosystemEvent | null
}

const MoonCatMomentDetail: NextPage<Props> = ({ moment, currentEvent }) => {
  const { status, owners } = useMomentOwners(moment)
  let imageSrc = moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
  const dateDetail = formatTimestamp(moment.eventDate)

  let ownersView: React.ReactNode
  if (status == 'pending') {
    ownersView = <LoadingIndicator />
  } else if (status == 'done') {
    ownersView = (
      <div style={{ columnWidth: '20em', marginBottom: '1.5em' }}>
        <table cellSpacing="0" cellPadding="0" className="zebra" style={{ margin: '0 auto' }}>
          <tbody>
            {owners.map((o) => {
              return (
                <tr key={o.tokenId.toString()}>
                  <td style={{ textAlign: 'center', lineHeight: 1 }}>
                    {o.moonCat ? (
                      <Link href={'/mooncats/' + o.moonCat}>
                        <a>
                          <picture>
                            <img
                              alt={'MoonCat ' + o.moonCat}
                              title={'MoonCat ' + o.moonCat}
                              src={`${API_SERVER_ROOT}/image/${o.moonCat}?scale=1&padding=1`}
                              style={{ height: '1.5em' }}
                            />
                          </picture>
                        </a>
                      </Link>
                    ) : (
                      ' '
                    )}
                  </td>
                  <td style={{ fontSize: '0.8rem' }}>
                    <EthereumAddress address={o.owner} />
                  </td>
                  <td style={{ padding: '0.2rem 1rem', textAlign: 'right' }}>#{Number(o.tokenId)}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    )
  }
  return (
    <div id="content-container">
      <Head>
        <title>{moment.meta.name}</title>
        <meta property="og:title" content={moment.meta.name} />
        <meta name="description" property="og:description" content={moment.meta.description} />
        <meta property="og:image" content={imageSrc} />
        <meta property="og:image:type" content="image/png" />
      </Head>
      <nav className="breadcrumb">
        <Link href="/moments">
          <a>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all Moments
          </a>
        </Link>
      </nav>
      <h1 className="hero">{moment.meta.name}</h1>
      <div>
        <picture>
          <img src={imageSrc} alt="" style={{ display: 'block', margin: '0 auto', maxHeight: '70vh' }} />
        </picture>
      </div>
      <div className="text-container">
        <section className="card">
          <p>
            <strong>Moment</strong> #{moment.momentId}, <strong>date</strong> {dateDetail}, <strong>issuance</strong>{' '}
            {moment.issuance}
          </p>

          <p>{moment.meta.description}</p>
          <p>
            <a href={`${moment.momentId}/claim`}>Claim eligible mints<Icon name="arrow-right" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} /></a>
          </p>
        </section>
      </div>
      <h2>Token Holders</h2>
      {ownersView}
      <h2>MoonCats Pictured</h2>
      <MoonCatGrid moonCats={moment.moonCats} apiPage="mooncats" isEventActive={currentEvent != null} />
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const moments: Moment[] = require('lib/moments_meta.json')
  if (typeof ctx.params == 'undefined') {
    console.log('Routing error')
    return { notFound: true }
  }
  const id = ctx.params.id
  if (Array.isArray(id) || typeof id == 'undefined') {
    console.error('Routing error', id)
    return { notFound: true }
  }

  const moment = moments.find((m) => m.momentId == Number(id))
  if (typeof moment == 'undefined') {
    console.error('No metadata for Moment', id)
    return { notFound: true }
  }

  // Fetch events listing from the API server
  const currentEvent = await getCurrentEvent()

  return { props: { moment, currentEvent } }
}

export default MoonCatMomentDetail
