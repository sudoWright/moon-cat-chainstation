import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { IPFS_GATEWAY, ZWS, formatTimestamp } from 'lib/util'
import { Moment } from 'lib/types'
const moments: Moment[] = require('lib/moments_meta.json')

const MomentDetail = ({ moment }: { moment: Moment }) => {
  let imageSrc = moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
  const dateDetail = formatTimestamp(moment.eventDate)
  return (
    <section style={{ display: 'flex', gap: '1rem', marginBottom: '4rem' }}>
      <div>
        <h2 style={{ marginTop: 0 }}>
          <Link href={'/moments/' + moment.momentId}>{moment.meta.name}</Link>
        </h2>
        <p className="subheading">
          Moment #{moment.momentId}, date: {dateDetail}, issuance: {moment.issuance}
        </p>
        <p>{moment.meta.description}</p>
      </div>
      <picture>
        <img alt="" style={{ flex: '0 0 auto', height: 150 }} src={imageSrc} />
      </picture>
    </section>
  )
}

const pageTitle = 'MoonCatMoments'
const MoonCatMoments: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Snapshots of important moments in the MoonCats lore!"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">MoonCat{ZWS}Moments</h1>
        <section className="card">
          <p>
            MoonCatMoments are snapshots into important moments in the MoonCats lore! Each MoonCat that participates in
            a Moment gets one copy of the NFT, so the number of any moment can be as little as one, or as many as
            hundreds or even thousand of copies.
          </p>
          <p>
            Owners of a MoonCatMoment can see a high-rez video file, as well as acquire a high-rez still purrfect for
            Ethereans looking to print and hang a little bit of MoonCats culture in their real-life spaces.
          </p>
          <p>
            <Link href="/moments/faq">More details...</Link>
          </p>
        </section>
        {moments.map((m) => (
          <MomentDetail key={m.momentId} moment={m} />
        ))}
      </div>
    </div>
  )
}
export default MoonCatMoments
