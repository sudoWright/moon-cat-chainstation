import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import { ZWS } from 'lib/util'

const pageTitle = 'lootprints'

const LootprintsHome: NextPage = () => {
  return (
    <div id="content-container">
      <Head>
        <title>lootprints</title>
        <meta property="og:title" content={pageTitle} />
        <meta
          name="description"
          property="og:description"
          content="Deep Space exploration vehicles, custom-tailored for MoonCats"
        />
      </Head>
      <div className="text-container">
        <h1 className="hero">lootprints</h1>
        <section className="card">
          <p>
            <strong>Hard Hats Required!</strong> This area of ChainStation Alpha is undergoing some intense
            construction. So, please pardon the dust, and check back a bit later to see what pops up here!
          </p>
        </section>
        <section className="card-notice">
          <p>
            While this site is under construction, more information about lootprints can be accessed{' '}
            <a href="https://mooncat.community/blog/lootprints">on the MoonCat{ZWS}Community website</a>.
          </p>
        </section>
      </div>
    </div>
  )
}
export default LootprintsHome
