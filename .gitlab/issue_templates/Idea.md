# Motivation
<!-- Describe the current state of things, and what logic lead to there’s a need/opportunity that could be taken advantage of? -->

# Questions To be Answered
<!-- Describe what needs to be done to figure out if this idea is actionable or not. This should likely be done as a bulleted list of questions, which can be filled in as disucssion happens. -->

# Resources
<!-- Additional links that would be helpful to explain the motivation or help research this idea. -->

<!-- Don’t change anything below this line -->
/label ~"Needs Research"