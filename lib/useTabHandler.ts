import { pageview } from 'lib/analytics'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

/**
 * Hook for encapsulating logic used by pages that use hash-navigation to flip between different "tabs" within the page content
 */
export default function useTabHandler(initialTab: string) {
  const router = useRouter()
  const [activeTab, setActiveTab] = useState<string>(initialTab)

  useEffect(() => {
    const hashValue = router.asPath.split('#')[1]
    if (typeof hashValue != 'undefined' && hashValue != '') {
      setActiveTab(hashValue.toLowerCase())
      pageview(router.asPath)
    }
  }, [router.asPath])

  const toggleTab = (tabId: string) => {
    if (activeTab != tabId) {
      // Update the hash value in the URL. This will cause router.asPath to update, and therefore the useEffect will fire again,
      // updating the internal state of which tab is active.
      router.replace({ hash: tabId }, undefined, { scroll: false })
    }
  }

  return { activeTab, toggleTab }
}
