import { CSSProperties, ChangeEventHandler } from 'react'
import { Address } from 'wagmi'

////////// MoonCats //////////

type MoonCatClassification = 'genesis' | 'rescue'
type HueName =
  | 'white'
  | 'black'
  | 'red'
  | 'orange'
  | 'yellow'
  | 'chartreuse'
  | 'green'
  | 'teal'
  | 'cyan'
  | 'skyblue'
  | 'blue'
  | 'purple'
  | 'magenta'
  | 'fuchsia'
type PaleFilter = 'no' | 'yes'
type Expression = 'smiling' | 'grumpy' | 'pouting' | 'shy'
type Facing = 'right' | 'left'
type Pattern = 'pure' | 'tabby' | 'spotted' | 'tortie'
type Pose = 'standing' | 'pouncing' | 'stalking' | 'sleeping'
type RescueYear = 2017 | 2018 | 2019 | 2020 | 2021
type NameFilter = 'no' | 'yes' | 'valid' | 'invalid'

export type MoonCatViewPreference = 'accessorized' | 'mooncat' | 'face' | 'event' | null

// Structure of data used as a local cache
export interface MoonCatData {
  rescueOrder: number
  rescueYear: RescueYear
  catId: string
  genesis?: true
  hueInt: number
  hueName: HueName
  pale: boolean
  facing: string
  expression: Expression
  pattern: Pattern
  pose: Pose
  nameRaw?: string
  name?: string | true
  namedOrder?: number
  namedYear?: number
}

interface ContractDetails {
  tokenId: number
  description: string
  address: Address
  capabilities: string[]
}

// Structure of data returned by API server for /owner-profile `ownedMoonCats` results
export interface OwnedMoonCat {
  rescueOrder: number
  catId: string
  collection: {
    name: string
    address: string
  }
}

// Structure of data returned by API server
export interface MoonCatDetails {
  rescueIndex: number
  catId: string // 0x-prefixed Hex
  name: string | null
  isNamed: 'Yes' | 'No' | 'Invalid UTF8' | 'Redacted'
  expression: Expression
  pose: Pose
  pattern: Pattern
  pale: boolean
  isPale: boolean
  facing: Facing

  rescuedBy?: Address
  rescueYear: number

  isAcclimated: boolean
  owner?: Address
  lootprint: 'Claimed' | 'Lost'
  contract: ContractDetails
  accessories: AccessoryDetails[]

  classification: MoonCatClassification
  genesis: boolean
  genesisGroup?: number

  hue: string
  hueValue: number
  kInt: number
  kBin: string // Binary value
  glow: number[]

  litterId: string // Hexadecimal value
  litter: number[]
  litterSize: number
  onlyChild: boolean

  twinId: string // Hexadecimal value
  twinSet: number[]
  twinSetSize: number
  hasTwins: false

  mirrorId: string // Hexadecimal value
  mirrorSet: number[]
  hasMirrors: boolean
  mirrorSetSize: number

  cloneId: string // Hexadecimal value
  cloneSet: number[]
  cloneSetSize: number
  hasClones: boolean
}

export interface MoonCatFilterSettings {
  rescueYear?: RescueYear
  classification?: MoonCatClassification
  hue?: HueName
  pale?: PaleFilter
  facing?: Facing
  expression?: Expression
  pattern?: Pattern
  pose?: Pose
  named?: NameFilter
  nameKeyword?: string
}

////////// Accessories //////////

export interface AccessoryData {
  id: number
}

// Structure of owned Accessory returned by API server
export interface AccessoryDetails {
  accessoryId: string // Note: integer expressed as string
  name: string
  displayName: string
  visible: boolean
}

////////// Moments //////////
export interface MomentMeta {
  description: string
  external_url: string
  image: string
  name: string
  attributes: { trait_type: string; value: string }[]
}

export interface Moment {
  momentId: number
  startingTokenId: number
  issuance: number
  tokenURI: string
  eventDate: number
  meta: MomentMeta
  tx: {
    hash: `0x${string}`
    blockNumber: number
    timestamp: number
    from: `0x${string}`
    to: `0x${string}`
  }
  moonCats: number[]
}

export interface MomentFilterSettings {
  momentId?: number
}

////////// Web App //////////

/**
 * Define the structure of form fields to assemble into a form
 */

interface BaseFieldMeta {
  name: string
  type: string
  label: string
}

export interface SelectFieldMeta extends BaseFieldMeta {
  type: 'select'
  defaultLabel?: string
  options: Record<string, string>
}
export interface TextFieldMeta extends BaseFieldMeta {
  type: 'text'
}

export function isSelectFieldMeta(m: SelectFieldMeta | TextFieldMeta): m is SelectFieldMeta {
  return m.type == 'select' && typeof (m as SelectFieldMeta).options != 'undefined'
}
export interface FieldProps<T extends BaseFieldMeta, E extends HTMLElement> {
  meta: T
  labelStyle: CSSProperties
  currentValue: any
  onChange: ChangeEventHandler<E>
}
