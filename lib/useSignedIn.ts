import { useContext } from "react";
import { AppVisitorContext, SIWE_LOGIN_STATEMENT, doUserCheck, useFetchStatus } from "./util";
import { SiweMessage } from "siwe";
import { useAccount, useNetwork, useSignMessage } from "wagmi";

export default function useSignedIn() {
  const { address } = useAccount()
  const { chain } = useNetwork()
  const { signMessageAsync } = useSignMessage()
  const [status, setStatus] = useFetchStatus()
  const {
    state: {
      verifiedAddresses: { value: verifiedAddresses },
    },
    dispatch
  } = useContext(AppVisitorContext)

  async function doSignIn(): Promise<boolean> {
    setStatus('pending')
    if (!address) {
      console.error('Not connected')
      setStatus('error')
      return false
    }

    const chainId = chain?.id
    if (!chainId) {
      console.error('No network detected')
      setStatus('error')
      return false
    }

    const nonceRes = await fetch('/api/nonce')
    const nonce = await nonceRes.text()

    const expires = new Date()
    expires.setTime(expires.getTime() + 24 * 60 * 60 * 1000) // Shift forward 24 hours

    const message = new SiweMessage({
      domain: window.location.host,
      address,
      statement: SIWE_LOGIN_STATEMENT,
      uri: window.location.origin,
      version: '1',
      chainId,
      nonce: nonce,
      expirationTime: expires.toISOString(),
    })
    let signature
    try {
      signature = await signMessageAsync({
        message: message.prepareMessage(),
      })
    } catch (err: any) {
      setStatus('error')
      if (err.name == 'UserRejectedRequestError') {
        console.warn('User rejected signing')
        return false
      }
      console.error('Signing error', err)
      return false
    }

    // Verify signature
    const verifyRes = await fetch('/api/siwe-verify', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message, signature }),
    })
    if (!verifyRes.ok) {
      setStatus('error')
      console.error('Fetch error', verifyRes)
      return false
    }
    let rs = await verifyRes.json()
    if (!rs.ok) {
      setStatus('error')
      console.error('Signature failed validation')
      return false
    }

    // Verification succeeded.
    doUserCheck(dispatch) // Refresh which addresses are verified now
    setStatus('done')
    return true
  }

  async function doLogOut() {
    setStatus('pending')
    await fetch('/api/logout')
    if (dispatch) doUserCheck(dispatch) // Refresh which addresses are verified now
    setStatus('done')
  }

  return {
    connectedAddress: address,
    verifiedAddresses,
    isConnected: typeof address != 'undefined',
    isSignedIn: verifiedAddresses.length > 0 && typeof address != 'undefined' && verifiedAddresses.includes(address),
    status,
    doSignIn,
    doLogOut
  }
}