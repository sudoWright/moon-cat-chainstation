import LoadingIndicator from 'components/LoadingIndicator'
import WarningIndicator from 'components/WarningIndicator'
import { useState } from 'react'
import { BaseError } from 'viem'
import { waitForTransaction, writeContract } from 'wagmi/actions'

type TxStatus = 'start' | 'building' | 'authorizing' | 'confirming' | 'done' | 'error'
export default function useTxStatus() {
  const [status, setStatus] = useState<TxStatus>('start')
  const [message, setMessage] = useState<React.ReactNode>(null)
  let viewMessage: React.ReactNode
  switch (status) {
    case 'building':
      viewMessage = <LoadingIndicator message="Building transaction..." />
      break
    case 'authorizing':
      viewMessage = <LoadingIndicator message="Authorizing transaction..." />
      break
    case 'confirming':
      viewMessage = <LoadingIndicator message="Awaiting blockchain confirmation..." />
      break
    case 'error':
      viewMessage = <WarningIndicator message={message as string} />
      break
    case 'done':
      viewMessage = message
  }
  function setStatusAndMessage(status: TxStatus, msg?: React.ReactNode) {
    if (typeof msg != 'undefined') {
      setMessage(msg)
    }
    setStatus(status)
  }
  async function processTransaction(config: Parameters<typeof writeContract>[0]) {
    setStatus('authorizing')
    let rs = writeContract(config)
    try {
      // Send transaction to be signed
      let { hash } = await rs
      setStatus('confirming')
      // Await confirmation in the blockchain
      await waitForTransaction({ hash })
      setStatus('done')
      return true
    } catch (err) {
      if (err instanceof BaseError) {
        console.error('Transaction failure', {
          name: err.name,
          shortMessage: err.shortMessage,
          cause: err.cause,
        })
        setStatusAndMessage('error', err.shortMessage)
      } else {
        console.log('Transaction unknown error', err)
        setStatusAndMessage('error', 'Transaction error')
      }
      return false
    }
  }
  return {
    status,
    viewMessage,
    setStatus: setStatusAndMessage,
    processTransaction,
  }
}
